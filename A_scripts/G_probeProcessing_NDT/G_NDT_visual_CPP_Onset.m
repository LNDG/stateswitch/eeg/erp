addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

ERPdata = [];
for indID = 1:numel(IDs)
    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v2/',IDs{indID},'_ERP.mat']);
    time = dataProbeAvg{2,1}.time;
    for indDim = 1:4
       for indChan = 1:60
            ERPdata(indID,indDim,indChan,:) = squeeze(nanmedian(dataProbeAvg{indDim,1}.avg(indChan,:),1));
       end
    end
end

%% load DDM estimates

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_YA_vt.mat')

DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)), cell2mat(cellfun(@str2num, IDs, 'un', 0)));
DDMIDidx = find(DDMIDidx);

%% event-lock to average (!) NDT in each condition
% This also works for individual NDTs but seems slightly noisier

ERPdata_NDTlocked = []; ERPdata_nonNDTlocked = [];
for indID = 1:numel(IDs)
    for indDim = 1:4
%        curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),indDim),1));
%        curNDT1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),1),1));
       curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),indDim),1));
       curNDT1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),1),1));
       timeIdx = find(time>1000.*(curNDT),1,'first');
       timeIdx_nonNDT = find(time>1000*curNDT1,1,'first');
       ERPdata_NDTlocked(indID,indDim,:,:) = ERPdata(indID,indDim,:,timeIdx-250:timeIdx+250);
       ERPdata_nonNDTlocked(indID,indDim,:,:) = ERPdata(indID,indDim,:,timeIdx_nonNDT-250:timeIdx_nonNDT+250);
    end
end

NewTime = -250*2:2:250*2;

% h = figure('units','normalized','position',[.1 .1 .4 .4]);
% subplot(2,2,2); cla; hold on;
%     for indCond = 1:4
%         plot(NewTime, squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,indCond,[54],:),3),1)), 'LineWidth', 3, 'Color', cBrew(indCond,:));
%     end
%     xlim([-500 500]); ylim([-10*10^-4 5*10^-4])
%     hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
%     xlabel('Time (ms from 1 Target NDT)'); ylabel('ERP amplitude (?V)')
%     title({'Grand average NDT-locked ERP @ POz (~ CPP)';''})
%     legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'SouthWest')
%     legend('boxoff');
%     meanNDTTime = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),1),1))*1000;
%     davinci('arrow', 'X', [-meanNDTTime -meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
%         abs(diff([4.8*10^-4 2*10^-4]).*.5),'Head.Width', 40, 'Shaft.Width', 10)
% subplot(2,2,4); cla; hold on;
%     for indCond = 1:4
%         plot(NewTime, squeeze(nanmean(nanmean(ERPdata_NDTlocked(:,indCond,[54],:),3),1)), 'LineWidth', 3, 'Color', cBrew(indCond,:));
%         meanNDTTime = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),indCond),1))*1000;
%         davinci('arrow', 'X', [-meanNDTTime -meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
%             abs(diff([4.8*10^-4 2*10^-4]).*.5), 'Head.Width', 40, 'Shaft.Width', 10, 'FaceColor', cBrew(indCond,:));
%     end
%     xlim([-500 500]); ylim([-10*10^-4 5*10^-4])
%     hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
%     xlabel('Time (ms from condition-specific NDT)'); ylabel('ERP amplitude (?V)')
%     title({'Grand average NDT-locked ERP @ POz (~ CPP)';''})
% subplot(2,2,1); cla; hold on;
%     for indCond = 1:4
%         plot(NewTime, squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,indCond,[58,60],:),3),1)), 'LineWidth', 3, 'Color', cBrew(indCond,:));
%     end
%     xlim([-500 500]); ylim([-5*10^-4 10*10^-4])
%     hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
%     xlabel('Time (ms from 1 Target NDT)'); ylabel('ERP amplitude (?V)')
%     title({'Grand average NDT-locked ERP @ O1/2 (~ N200)';''})
%     meanNDTTime = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),1),1))*1000;
%     davinci('arrow', 'X', [-meanNDTTime -meanNDTTime], 'Y', [9.5*10^-4 7*10^-4], 'ArrowType', 'single', 'Head.Length', ...
%         abs(diff([9.5*10^-4 7*10^-4]).*.5),'Head.Width', 40, 'Shaft.Width', 10)
% subplot(2,2,3); cla; hold on;
%     for indCond = 1:4
%         plot(NewTime, squeeze(nanmean(nanmean(ERPdata_NDTlocked(:,indCond,[58,60],:),3),1)), 'LineWidth', 3, 'Color', cBrew(indCond,:));
%         meanNDTTime = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),indCond),1))*1000;
%         davinci('arrow', 'X', [-meanNDTTime -meanNDTTime], 'Y', [9.5*10^-4 7*10^-4], 'ArrowType', 'single', 'Head.Length', ...
%             abs(diff([9.5*10^-4 7*10^-4]).*.5),'Head.Width', 40, 'Shaft.Width', 10, 'FaceColor', cBrew(indCond,:));
%     end
%     xlim([-500 500]); ylim([-5*10^-4 10*10^-4])
%     hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
%     xlabel('Time (ms from condition-specific NDT)'); ylabel('ERP amplitude (?V)')
%     title({'Grand average NDT-locked ERP @ O1/2 (~ N200)';''})
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/';
% figureName = 'G_NDT_IntegrationOnset';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

%% plot only CPP onset

h = figure('units','normalized','position',[.1 .1 .2 .4]);
set(gcf,'renderer','Painters')
subplot(2,1,1); cla; hold on;
    for indCond = 1:4
        % new value = old value ?? subject average + grand average
        time = NewTime;
        condAvg = squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(:,1:4,[54],:),3),2));
        curData = squeeze(nanmean(ERPdata_nonNDTlocked(:,indCond,[54],:),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        ll{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(indCond,:),'linewidth', 3}, 'patchSaturation', .1);
    end
    xlim([-500 500]); ylim([-5*10^-4 13*10^-4])
    hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
    xlabel('Time (ms from 1 Target NDT)'); ylabel('ERP amplitude (?V)')
    title({'Grand average NDT-locked CPP';''})
    meanNDTTime = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),1),1))*1000;
    davinci('arrow', 'X', [-meanNDTTime -meanNDTTime], 'Y', [4.8*10^-4 2*10^-4], 'ArrowType', 'single', 'Head.Length', ...
        abs(diff([4.8*10^-4 2*10^-4]).*.5),'Head.Width', 40, 'Shaft.Width', 10)
    legend([ll{1}.mainLine, ll{2}.mainLine, ll{3}.mainLine, ll{4}.mainLine], ...
        {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'SouthEast')
    legend('boxoff');
subplot(2,1,2); cla; hold on;
    for indCond = 1:4
        % new value = old value ?? subject average + grand average
        time = NewTime;
        condAvg = squeeze(nanmean(nanmean(ERPdata_NDTlocked(:,1:4,[54],:),3),2));
        curData = squeeze(nanmean(ERPdata_NDTlocked(:,indCond,[54],:),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        ll{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(indCond,:),'linewidth', 3}, 'patchSaturation', .1);
        meanNDTTime = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(:),indCond),1))*1000;
        davinci('arrow', 'X', [-meanNDTTime -meanNDTTime], 'Y', [4.8*10^-4 2*10^-4]+5*10^-4, 'ArrowType', 'single', 'Head.Length', ...
            abs(diff([4.8*10^-4 2*10^-4]).*.5), 'Head.Width', 40, 'Shaft.Width', 10, 'FaceColor', cBrew(indCond,:));
    end
    xlim([-500 500]);ylim([-5*10^-4 13*10^-4])
    hold on; line([0 0],get(gca,'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
    xlabel('Time (ms from condition-specific NDT)'); ylabel('ERP amplitude (?V)')
    %title({'Grand average NDT-locked CPP';''})
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/';
figureName = 'G_NDT_IntegrationOnset';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');



restoredefaultpath;

pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
addpath('/Volumes/Kosciessa/Tools/ploterr')
addpath('/Volumes/Kosciessa/Tools/mysigstar')

pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v5/';
load([pn.out, 'GrandAverages_v5.mat'], 'dataResponseGrandAvg')

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

h = figure('units','normalized','position',[.1 .1 .4 .35]);
cla; hold on;
    
    cBrew = brewermap(4,'RdBu');
    cBrew = flipud(cBrew);

    catAll = cat(4,dataResponseGrandAvg{1,1}.individual,...
    dataResponseGrandAvg{2,1}.individual,dataResponseGrandAvg{3,1}.individual,...
    dataResponseGrandAvg{4,1}.individual);

% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);

    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
            {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'South', 'Orientation', 'horizontal'); legend('boxoff');
    xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (µV)');
    title({'CPP slope decreases with prior target uncertainty'; ''})

%% inset 1: bar plots of slope decrease with load
    
    dataForSlopeFit = []; slopes = [];
    for indAge = 1:2
        for indCond = 1:4
            dataForSlopeFit{indAge}(:,indCond,:) = squeeze(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<-50));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
            end
        end
    end
    
    condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
    condPairsLevel = [8.3*10^-6 8.9*10^-6 7.75*10^-6 9.3*10^-6 5*10^-6 10.5*10^-6];
        
    axes('Position',[.2 .55 .18 .25])
    box off
    
    hold on;
        dat = squeeze(slopes{1}(:,:,1));
        bar(1:4, nanmean(dat), 'FaceColor',  cBrew(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % show standard deviation on top
        h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
        set(h1(1), 'marker', 'none'); % remove marker
        set(h1(2), 'LineWidth', 4);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
            'xlim', [0.5 4.5]); ylim([-1*10^-6 10.5*10^-6])
        ylabel('CPP slope'); xlabel('# of targets');
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
                mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
            end
        end
        
 %% inset 2: topography
 
    axes('Position',[.8 .2 .3 .3])
    box off
 
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.zlim = [-3*10^-4 6*10^-4];
    cfg.colormap = cBrew;
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<100;
    grandAvg = cat(4, dataResponseGrandAvg{1,1}.individual, dataResponseGrandAvg{2,1}.individual, ...
        dataResponseGrandAvg{3,1}.individual, dataResponseGrandAvg{4,1}.individual);
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(grandAvg(:,:,idxTime,:),3),1),4))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude (µV)');
    %title({'ERP (across loads): -250:100 ms peri-response'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    
%% save CPP figure

% pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
% figureName = 'Z3_CPP_YA';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');
%   
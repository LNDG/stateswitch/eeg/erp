%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v5/';

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)
    load([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
    %ERPstruct.dataStimAvg(:,id) = dataStimAvg(:,1);
    ERPstruct.dataProbeAvg(:,id) = dataProbeAvg(:,1);
    ERPstruct.dataResponseAvg(:,id) = dataResponseAvg(:,1);
end

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1:2
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        %dataStimGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,ageIdx{indAge}});
        dataProbeGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg{indCond,ageIdx{indAge}});
        dataResponseGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg{indCond,ageIdx{indAge}});
    end
end

% save grand averages

save([pn.out, 'GrandAverages_v5.mat'], 'dataProbeGrandAvg', 'dataResponseGrandAvg')

%% load grand averages

load([pn.out, 'GrandAverages_v5.mat'], 'dataProbeGrandAvg', 'dataResponseGrandAvg')
%load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/GrandAverages_v3.mat'], 'dataProbeGrandAvg', 'dataResponseGrandAvg')

%% overview plot

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
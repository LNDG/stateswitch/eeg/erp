% Calculate ERPs across subjects by Run of the STSW task.

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)

    display(['processing ID ' IDs{id}]);

    %% load data

    load([pn.dataIn, IDs{id}, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

    %% protect TrlInfo from FT
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    %% CSD transform (compute prior to averaging)
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    
    %% 40 Hz low-pass filter
    
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = 40;
    cfg.lpfiltord = 6;
    cfg.lpfilttype = 'but';
    cfg.lpfiltdir = 'twopass';
    
    data = ft_preprocessing(cfg,data);
    
    %% baseline: average of -600 to -500 ms prior to stim onset
    
    cfg = [];
    cfg.baseline = [2.4 2.5];
    [data] = ft_timelockbaseline(cfg, data);
        
%% timelock to accuracy
    
    dataStimAvg = cell(4,2);
    for indCond = 1:4
        for indAcc = 1:2
            cfg = [];
            cfg.trials = TrlInfo(:,8)==indCond & TrlInfo(:,10)==indAcc-1;
            N = numel(cfg.trials);
            cfg.latency = [2 7];
            dataStim = ft_selectdata(cfg, data);
            % timelock to stim onset
            cfg = [];
            dataStimAvg{indCond,indAcc} = ft_timelockanalysis(cfg, dataStim);
            dataStimAvg{indCond,indAcc}.N = N;
        end
    end
        
    %% save computed files

    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/H_ERPbyAcc/';
    save([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg');
    
end % ID


% Plot early visual potentials and assess ERPs during stimulus processing

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v2/';
    load([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
    ERPstruct.dataStimAvg(:,id) = dataStimAvg(:,1);
    ERPstruct.dataProbeAvg(:,id) = dataProbeAvg(:,1);
    ERPstruct.dataResponseAvg(:,id) = dataResponseAvg(:,1);
end

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1:2
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        dataStimGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,ageIdx{indAge}});
        dataProbeGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg{indCond,ageIdx{indAge}});
        dataResponseGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg{indCond,ageIdx{indAge}});
    end
end

%% Stimulus-evoked SSVEP

h = figure('units','normalized','position',[.1 .1 .9 .4]);
subplot(1,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([-1000 3000]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
subplot(1,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([-1000 3000]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: Occipital ERP')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/I/'; mkdir(pn.plotFolder);
figureName = 'I_occipitalERP_STIM';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% restrict to first 500 ms

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
    line([50,50], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([100,100], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([130,130], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([160,160], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([180,180], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([220,220], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([280,280], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
    line([320,320], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
subplot(1,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    xlim([0 500]);
    title('OA: Occipital ERP')
     line([50,50], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([100,100], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 0 0])
    line([130,130], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([160,160], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [1 .2 .2])
    line([180,180], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([220,220], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [0 .8 .5])
    line([280,280], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
    line([320,320], [-1.7*10^-3,1*10^-3], 'LineStyle', '--', 'Color', [.2 .2 1])
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/I/'; mkdir(pn.plotFolder);
figureName = 'I_occipitalERP_STIM_Early';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot topography 

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

% add colorbrewer
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';
cfg.style = 'both';
cfg.colormap = cBrew;

%% Early stimulus-evoked potentials

h = figure('units','normalized','position',[.1 .1 .7 .7]);
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    curData = squeeze(nanmean(cat(4, dataStimGrandAvg{1,1}.individual,dataStimGrandAvg{2,1}.individual,...
        dataStimGrandAvg{3,1}.individual, dataStimGrandAvg{4,1}.individual),4));
subplot(2,3,1)
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>50 & dataStimGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 50:100 ms stim')
subplot(2,3,2)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>130 & dataStimGrandAvg{1,1}.time<160;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 130:160 ms stim')
subplot(2,3,3)
    cfg.zlim = [-6*10^-4 6*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>180 & dataStimGrandAvg{1,1}.time<220;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 180:220 ms stim')
subplot(2,3,4)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>280 & dataStimGrandAvg{1,1}.time<320;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 280:320 ms stim')
subplot(2,3,5)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>320 & dataStimGrandAvg{1,1}.time<1000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 320:1000 ms stim')
subplot(2,3,6)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>2000 & dataStimGrandAvg{1,1}.time<3000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ 2000:3000 ms stim')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/I/'; mkdir(pn.plotFolder);
figureName = 'I1_topoEarlyERP_YA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% Early potentials for OAs

h = figure('units','normalized','position',[.1 .1 .7 .7]);
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    curData = squeeze(nanmean(cat(4, dataStimGrandAvg{1,2}.individual,dataStimGrandAvg{2,2}.individual,...
        dataStimGrandAvg{3,2}.individual, dataStimGrandAvg{4,2}.individual),4));
subplot(2,3,1)
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>50 & dataStimGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 50:100 ms stim')
subplot(2,3,2)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>130 & dataStimGrandAvg{1,1}.time<160;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 130:160 ms stim')
subplot(2,3,3)
    cfg.zlim = [-6*10^-4 6*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>180 & dataStimGrandAvg{1,1}.time<220;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 180:220 ms stim')
subplot(2,3,4)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>280 & dataStimGrandAvg{1,1}.time<320;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 280:320 ms stim')
subplot(2,3,5)
    cfg.zlim = [-10*10^-4 10*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>320 & dataStimGrandAvg{1,1}.time<1000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 320:1000 ms stim')
subplot(2,3,6)
    cfg.zlim = [-8*10^-4 8*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>2000 & dataStimGrandAvg{1,1}.time<3000;
     plotData.powspctrm = squeeze(nanmean(nanmean(curData(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ 2000:3000 ms stim')
    
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/I/'; mkdir(pn.plotFolder);
figureName = 'I1_topoEarlyERP_OA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% parametric increase around 300 ms

h = figure('units','normalized','position',[.1 .1 .7 .8]);
subplot(2,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[58:60],:)-dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[58:60],:)-dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],:)-dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
     xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
     legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
subplot(2,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[58:60],:)-dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[58:60],:)-dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[58:60],:)-dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
    legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    xlim([0 500]);
    title('OA: Occipital ERP')
subplot(2,2,3);
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>200 & dataStimGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,:,idxTime)-dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP 4-1: 200:300 ms')
subplot(2,2,4);
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>200 & dataStimGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,:,idxTime)-dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP 4-1: 200:300 ms')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/I/'; mkdir(pn.plotFolder);
figureName = 'I_occipitalERP_STIM_Early_modulation';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot early ERP increase for YA
    
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/';
addpath([pn.root, 'T_tools/brewermap']) % add colorbrewer
addpath([pn.root, 'T_tools/shadedErrorBar']); % add shadedErrorBar
addpath([pn.root, 'T_tools/barwitherr/']); % add barwitherr
addpath([pn.root, 'T_tools/']); % add convertPtoExponential

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);

dataStimGrandAvgTotal = cat(4, dataStimGrandAvg{1,1}.individual, dataStimGrandAvg{2,1}.individual, ...
    dataStimGrandAvg{3,1}.individual, dataStimGrandAvg{4,1}.individual);

% h = figure('units','normalized','position',[.1 .1 .4 .4]);
% hold on;
%     condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,:),2),4));
%     curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,1),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
%     
%     condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,:),2),4));
%     curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l2 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(2,:),'linewidth', 1}, 'patchSaturation', .25);
%     
%     condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,:),2),4));
%     curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,3),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l3 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(3,:),'linewidth', 1}, 'patchSaturation', .25);
%     
%     condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,:),2),4));
%     curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,4),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l4 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
    
h = figure('units','normalized','position',[.1 .1 .7 .6]);
subplot(2,1,1); cla;
hold on;
    %condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,:),2),4));
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,1),2));
    %curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([-1000 3100])
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude (?Volt)')
	legend([l1.mainLine], {'Viusal ERP Load 1'}); legend('boxoff');
subplot(2,1,2);
%h = figure('units','normalized','position',[.1 .1 .7 .3]);
hold on;
    condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2:4)-repmat(dataStimGrandAvgTotal(:,[58:60],:,1),1,1,1,3),2),4));
%     curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,1)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,3)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,4)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    
    % load linear regression statistics
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')

    significantLinear = stat.mask;
    significantLinear = significantLinear*-1.5*10^-4;
    significantLinear(significantLinear==0) = NaN;
    plot(stat.time, significantLinear, 'k', 'LineWidth', 5)
    xlim([-1000 3100])
    legend([l2.mainLine, l3.mainLine, l4.mainLine], {'Load 2 - Load 1'; 'Load 3 - Load 1'; 'Load 4 - Load 1'}); legend('boxoff');
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude difference to selective attention')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/';
figureName = 'I_stimERPmodulation';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot baseline topographies

%% plot change topography

figure
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-3*10^-4 3*10^-4];
    idxTime = dataStimGrandAvg{1,1}.time>100 & dataStimGrandAvg{1,1}.time<1000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,:,idxTime)-dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP 4-1: 100:1000 ms')
    
%% check for correlations with PC1

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/Z2_EEGAttentionFactor.mat')

idx_time = dataStimGrandAvg{1,1}.time>200 & dataStimGrandAvg{1,1}.time<300;
y = squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],idx_time)-dataStimGrandAvg{1,1}.individual(:,[58:60],idx_time),3),2));

figure; 
idx_time = dataStimGrandAvg{1,1}.time>180 & dataStimGrandAvg{1,1}.time<220;
y = squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],idx_time),3)-nanmean(dataStimGrandAvg{1,1}.individual(:,[59],idx_time),3),2));
scatter(EEGAttentionFactor.PCAalphaGamma, y, 'filled');
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, y)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/Z2_EEGAttentionFactor_OA.mat')

figure; 
idx_time = dataStimGrandAvg{1,1}.time>0 & dataStimGrandAvg{1,1}.time<3000;
y = squeeze(nanmedian(nanstd(dataStimGrandAvg{4,2}.individual(:,[44:50],idx_time),[],3)-nanstd(dataStimGrandAvg{1,2}.individual(:,[44:50],idx_time),[],3),2));
scatter(EEGAttentionFactor.PCAalphaGamma, y, 'filled');
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, y)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/Z2_EEGAttentionFactor_YAOA.mat')


figure; 
idx_time = dataStimGrandAvg{1,1}.time>220 & dataStimGrandAvg{1,1}.time<300;
y = [squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],idx_time),3)-nanmean(dataStimGrandAvg{1,1}.individual(:,[59],idx_time),3),2));...
    squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[59],idx_time),3)-nanmean(dataStimGrandAvg{1,2}.individual(:,[59],idx_time),3),2))];
scatter(EEGAttentionFactor.PCAalphaGamma, y, 'filled');
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, y)

%% CBPA for event-related signature

%% tmp plots

h = figure('units','normalized','position',[.1 .1 .7 .8]);
subplot(2,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[59],:)-dataStimGrandAvg{1,1}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[59],:)-dataStimGrandAvg{1,1}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],:)-dataStimGrandAvg{1,1}.individual(:,[59],:),2),1)), 'LineWidth', 2)
     xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
     legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
subplot(2,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[59],:)-dataStimGrandAvg{1,2}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[59],:)-dataStimGrandAvg{1,2}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[59],:)-dataStimGrandAvg{1,2}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
    legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    xlim([0 500]);
    title('OA: Occipital ERP')

h = figure('units','normalized','position',[.1 .1 .4 .4]);

imagesc(dataStimGrandAvg{1,1}.time,[],squeeze(nanmean((nanmean(dataStimGrandAvg{1,1}.individual(:,[43:50],:),2)-(dataStimGrandAvg{1,1}.individual(:,[59],:))-nanmean(dataStimGrandAvg{1,1}.individual(:,[58,60],:),2)),2)))
    
figure; hold on;
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean((nanmean(dataStimGrandAvg{1,1}.individual(:,[43:50],:),2)+(dataStimGrandAvg{1,1}.individual(:,[59],:))+nanmean(dataStimGrandAvg{1,1}.individual(:,[58,60],:),2)),1))))
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean((nanmean(dataStimGrandAvg{2,1}.individual(:,[43:50],:),2)+(dataStimGrandAvg{2,1}.individual(:,[59],:))+nanmean(dataStimGrandAvg{2,1}.individual(:,[58,60],:),2)),1))))
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean((nanmean(dataStimGrandAvg{3,1}.individual(:,[43:50],:),2)+(dataStimGrandAvg{3,1}.individual(:,[59],:))+nanmean(dataStimGrandAvg{3,1}.individual(:,[58,60],:),2)),1))))
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean((nanmean(dataStimGrandAvg{4,1}.individual(:,[43:50],:),2)+(dataStimGrandAvg{4,1}.individual(:,[59],:))+nanmean(dataStimGrandAvg{4,1}.individual(:,[58,60],:),2)),1))))

    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{2,1}.individual(:,[43:50],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{3,1}.individual(:,[43:50],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{4,1}.individual(:,[43:50],:),2),2),1)))
    

subplot(2,2,2); hold on;

    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{4,1}.individual(:,[43:50],:),2),1))-...
        squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[59],:)-dataStimGrandAvg{1,1}.individual(:,[43:50],:),2),1)))
    cla;
    
    figure; hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{1,1}.individual(:,[43:50],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{2,1}.individual(:,[43:50],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{3,1}.individual(:,[43:50],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{4,1}.individual(:,[43:50],:),2),2),1)))
    
    figure; hold on;
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[1:60],:),2),1))))
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[1:60],:),2),1))))
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[1:60],:),2),1))))
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[1:60],:),2),1))))
    
    figure; hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanstd(dataStimGrandAvg{1,1}.individual(:,[1:60],:),[],2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanstd(dataStimGrandAvg{2,1}.individual(:,[1:60],:),[],2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanstd(dataStimGrandAvg{3,1}.individual(:,[1:60],:),[],2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanstd(dataStimGrandAvg{4,1}.individual(:,[1:60],:),[],2),1)))

    
    figure; hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{1,1}.individual(:,[58,60],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{2,1}.individual(:,[58,60],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{3,1}.individual(:,[58,60],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],:)-nanmean(dataStimGrandAvg{4,1}.individual(:,[58,60],:),2),2),1)))
    
    figure; hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[59],:)-nanmean(dataStimGrandAvg{1,2}.individual(:,[58,60],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[59],:)-nanmean(dataStimGrandAvg{2,2}.individual(:,[58,60],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[59],:)-nanmean(dataStimGrandAvg{3,2}.individual(:,[58,60],:),2),2),1)))
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[59],:)-nanmean(dataStimGrandAvg{4,2}.individual(:,[58,60],:),2),2),1)))
    
    
    
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[59],:)-dataStimGrandAvg{1,1}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[59],:)-dataStimGrandAvg{1,1}.individual(:,[59],:),2),1)), 'LineWidth', 2)
     xlim([0 500]); xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
     legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
subplot(2,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[59],:)-dataStimGrandAvg{1,2}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[59],:)-dataStimGrandAvg{1,2}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[59],:)-dataStimGrandAvg{1,2}.individual(:,[59],:),2),1)), 'LineWidth', 2)
    xlabel('Time (ms); stim-locked'); ylim([-0.3*10^-3 0.3*10^-3])
    legend({'2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    xlim([0 500]);
    title('OA: Occipital ERP')


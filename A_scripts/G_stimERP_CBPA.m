%% Introduction

% Compute a CBPA after averaging over occipital channels (O1, O2, Oz).
% Linear regression.

%% setup

restoredefaultpath;

addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/fieldtrip-20170904']); ft_defaults

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v2/';
    load([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg');
    ERPstruct.dataStimAvg(:,id) = dataStimAvg(:,1);
end

elec = dataStimAvg{1}.elec;

%% CBPA

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label;

cfgStat = [];
cfgStat.channel          = {'O1', 'O2', 'Oz'};
cfgStat.avgoverchan      = 'yes';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
%cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'avg';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, ERPstruct.dataStimAvg{1});

subj = size(ERPstruct.dataStimAvg,2);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat] = ft_timelockstatistics(cfgStat, ...
    ERPstruct.dataStimAvg{1,:}, ERPstruct.dataStimAvg{2,:}, ERPstruct.dataStimAvg{3,:}, ERPstruct.dataStimAvg{4,:});

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')

%% plot output

figure; plot(stat.time, stat.mask.*stat.stat)

%% check the topographical specificity

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label;

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.latency          = [100 1000];
cfgStat.avgovertime      = 'yes';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
%cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'avg';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, ERPstruct.dataStimAvg{1});

subj = size(ERPstruct.dataStimAvg,2);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_topo] = ft_timelockstatistics(cfgStat, ...
    ERPstruct.dataStimAvg{1,:}, ERPstruct.dataStimAvg{2,:}, ERPstruct.dataStimAvg{3,:}, ERPstruct.dataStimAvg{4,:});

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA_topo.mat', 'stat_topo', 'cfgStat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA_topo.mat', 'stat_topo', 'cfgStat')

%% Figure: results of CBPA

h = figure('units','normalized','position',[.1 .1 .3 .3]);
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg = [];
    cfg.zlim = [-7 7];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.highlight = 'on';
    cfg.highlightchannel = plotData.label(stat_topo.mask);
    cfg.highlightcolor = [1 1 1];
    cfg.highlightsize = 8;
    plotData.powspctrm = stat_topo.stat;
    ft_topoplotER(cfg,plotData);
    title('Increases in Stimulus-evoked ERP: 100:1000 ms')
    cb = colorbar; set(get(cb,'title'),'string','t values');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/';
figureName = 'G_ERPIncreaseTopography';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

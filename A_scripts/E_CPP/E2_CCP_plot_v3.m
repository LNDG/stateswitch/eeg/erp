%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

% for id = 1:length(IDs)
%     load([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
%     ERPstruct.dataStimAvg(:,id) = dataStimAvg(:,1);
%     ERPstruct.dataProbeAvg(:,id) = dataProbeAvg(:,1);
%     ERPstruct.dataResponseAvg(:,id) = dataResponseAvg(:,1);
% end

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

% cfg = [];
% for indAge = 1:2
%     for indCond = 1:4
%         cfg.keepindividual = 'yes';
%         dataStimGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,ageIdx{indAge}});
%         dataProbeGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg{indCond,ageIdx{indAge}});
%         dataResponseGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg{indCond,ageIdx{indAge}});
%     end
% end
% 
% % save grand averages
% 
% save([pn.out, 'GrandAverages_v3.mat'], 'dataStimGrandAvg', 'dataProbeGrandAvg', 'dataResponseGrandAvg')

%% load grand averages

load([pn.out, 'GrandAverages_v3.mat'], 'dataStimGrandAvg', 'dataProbeGrandAvg', 'dataResponseGrandAvg')

%% Stimulus-evoked SSVEP

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([0 3500]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: Occipital ERP')
subplot(1,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[58:60],:),2),1)), 'LineWidth', 2)
    xlim([0 3500]); xlabel('Time (ms); stim-locked'); ylim([-1.7*10^-3 1*10^-3])
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
%     legend({'YA: 1 Target'; 'YA: 2 Targets'; 'YA: 3 Targets'; 'YA: 4 Targets';...
%         'OA: 1 Target'; 'OA: 2 Targets'; 'OA: 3 Targets'; 'OA: 4 Targets'}); legend('boxoff')
    title('OA: Occipital ERP')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
% pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/'; mkdir(pn.plotFolder);
% figureName = 'E2_occipitalERP_STIM';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

    
%% Overview plots

figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{1,1}.individual,1))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataResponseGrandAvg{1,1}.individual,1))); title('Response-locked')

figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataStimGrandAvg{1,1}.individual,1))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataResponseGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataResponseGrandAvg{1,1}.individual,1))); title('Response-locked')


figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{2,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{3,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Response-locked')


figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{1,1}.individual(:,46,:),2))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),2))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),2))); title('Response-locked')

figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,2}.time, [], squeeze(nanmean(dataStimGrandAvg{1,2}.individual(:,46,:),2))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,2}.time, [], squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),2))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,2}.time, [], squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),2))); title('Response-locked')


% figure; 
% subplot(1,3,1); imagesc(dataStimGrandAvg{1}.time, [], dataStimGrandAvg{1}.avg); title('Stim-locked')
% subplot(1,3,2); imagesc(dataProbeGrandAvg{1}.time, [], dataProbeGrandAvg{1}.avg); title('Probe-locked')
% subplot(1,3,3); imagesc(dataResponseGrandAvg{1}.time, [], dataResponseGrandAvg{1}.avg); title('Response-locked')


h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3) relative to stimulus onset')
subplot(2,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P6) relative to stimulus onset')
subplot(2,2,3); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3) relative to stimulus onset')
subplot(2,2,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P6) relative to stimulus onset')
    
%% plot topography 

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-3 3];

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,1)
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,2)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,1}.time>200 & dataProbeGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,3)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,3,4)
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,2}.time>1000 & dataStimGrandAvg{1,2}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,5)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,2}.time>200 & dataProbeGrandAvg{1,2}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,6)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,2}.time>-100 & dataResponseGrandAvg{1,2}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    %% 4-1 contrast
    
figure;
subplot(2,3,1)
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,:,idxTime)-dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,2)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,1}.time>200 & dataProbeGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{4,1}.individual(:,:,idxTime)-dataProbeGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,3)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{4,1}.individual(:,:,idxTime)-dataResponseGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,3,4)
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,2}.time>1000 & dataStimGrandAvg{1,2}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,:,idxTime)-dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,5)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,2}.time>200 & dataProbeGrandAvg{1,2}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{4,2}.individual(:,:,idxTime)-dataProbeGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,6)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,2}.time>-100 & dataResponseGrandAvg{1,2}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{4,2}.individual(:,:,idxTime)-dataResponseGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
%% YA vs OA contrast

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    xlim([-200 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target YA'; '4 Targets YA'; '1 Target OA'; '4 Targets OA'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(1,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(1,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),2),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{4,1}.individual(:,46,:),2),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),2),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{4,2}.individual(:,46,:),2),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% cumsum

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% Check for potential integration onset differences

timeIntervals = [150 200 250 300 350 400 450];
for indCond = 1:4
    figure;
    for indTime = 1:numel(timeIntervals)-1
        subplot(1,numel(timeIntervals)-1, indTime);
        plotData = [];
        plotData.label = dataProbeGrandAvg{indCond,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxTime = dataProbeGrandAvg{indCond,1}.time>timeIntervals(indTime) & ...
            dataProbeGrandAvg{indCond,1}.time<timeIntervals(indTime+1);
        plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{indCond,1}.individual(:,:,idxTime),3),1))';
        ft_topoplotER(cfg,plotData);
        %title('YA ERP @ +200:+300 probe, 1 Target')
    end
end


figure;
subplot(2,3,1)
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,2)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,1}.time>200 & dataProbeGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,3)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,3,4)
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,2}.time>1000 & dataStimGrandAvg{1,2}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,5)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,2}.time>300 & dataProbeGrandAvg{1,2}.time<350;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,6)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,2}.time>-100 & dataResponseGrandAvg{1,2}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
%% CPP plot at maximum positive and negative CPP locations
    
h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{1,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{2,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{3,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{4,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ POz) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,54,:)-dataProbeGrandAvg{1,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,54,:)-dataProbeGrandAvg{2,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,54,:)-dataProbeGrandAvg{3,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,54,:)-dataProbeGrandAvg{4,1}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ POz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,54,:)-dataResponseGrandAvg{1,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,54,:)-dataResponseGrandAvg{2,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,54,:)-dataResponseGrandAvg{3,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,54,:)-dataResponseGrandAvg{4,1}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ POz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{1,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{2,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{3,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{4,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ POz) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,54,:)-dataProbeGrandAvg{1,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,54,:)-dataProbeGrandAvg{2,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,54,:)-dataProbeGrandAvg{3,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,54,:)-dataProbeGrandAvg{4,2}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ POz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,54,:)-dataResponseGrandAvg{1,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,54,:)-dataResponseGrandAvg{2,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,54,:)-dataResponseGrandAvg{3,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,54,:)-dataResponseGrandAvg{4,2}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ POz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ Cz) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Cz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Cz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ Cz) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Cz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Cz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% plot YA and OA in same plot

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,3,1); hold on;
    indChannel = 29;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA + OA: Negative CPP (@ Cz) relative to response')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,3,2); hold on;
    indChannel = 54;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    line([0 0], get(gca, 'Ylim'))
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA + OA: Positive CPP (@ POz) relative to response')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,3,3); hold on;
    indChannel1 = 54;
    indChannel2 = 29;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{1,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{2,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{3,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{4,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{1,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{2,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{3,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{4,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA + OA: Positive-Negative CPP')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% compare topographies between -400,-200 and -200,0

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-3 3];

    figure;
    subplot(2,4,3);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-400 & dataResponseGrandAvg{1,1}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 4 -400 to -200')
    
    subplot(2,4,4);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 4 -200 to 0')
    
    subplot(2,4,1);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-400 & dataResponseGrandAvg{1,1}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 1 -400 to -200')
    
    subplot(2,4,2);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 1 -200 to 0')
    
   
 %% OA
 
    subplot(2,4,7);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-400 & dataResponseGrandAvg{1,2}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 4 -400 to -200')
    
    subplot(2,4,8);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-200 & dataResponseGrandAvg{1,2}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 4 -200 to 0')
    
    subplot(2,4,5);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-400 & dataResponseGrandAvg{1,2}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 1 -400 to -200')
    
    subplot(2,4,6);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-200 & dataResponseGrandAvg{1,2}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 1 -200 to 0')

   
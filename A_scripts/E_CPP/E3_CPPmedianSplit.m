    %% Probe association between baseline and change in CPP

    restoredefaultpath;
    clear all; close all; pack; clc;


    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
    pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';

    addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;


    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
    ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

    load([pn.out, 'GrandAverages_v3.mat'], 'dataStimGrandAvg', 'dataProbeGrandAvg', 'dataResponseGrandAvg')

    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_v12_a2_t2.mat')
    
    idx_YA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{1}));
    idx_OA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{2}));
    
    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    subplot(1,3,1); hold on;
        l1 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), 'filled'); lsline();
        [r1, p1] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2));
        l2 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,3), 'filled'); lsline();
        [r2, p2] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,3));
        l3 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4), 'filled'); lsline();
        [r3, p3] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4));
        legend([l1,l2,l3],{['load2: r=',num2str(round(r1(2),2)), ', p=',num2str(round(p1(2),2))],...
            ['load3: r=',num2str(round(r2(2),2)), ', p=',num2str(round(p2(2),2))],...
            ['load4: r=',num2str(round(r3(2),2)), ', p=',num2str(round(p3(2),2))]}, 'location', 'NorthWest'); legend('boxoff');
        xlabel('Load 1 Drift'); ylabel('Load X drift (see label)')
    subplot(1,3,2); hold on;
        l1 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), 'filled'); lsline();
        [r1, p1] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1));
        l2 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), 'filled'); lsline();
        [r2, p2] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2));
        l3 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,3), 'filled'); lsline();
        [r3, p3] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,3));
        legend([l1,l2,l3],{['load4-1: r=',num2str(round(r1(2),2)), ', p=',num2str(round(p1(2),2))],...
            ['load4-2: r=',num2str(round(r2(2),2)), ', p=',num2str(round(p2(2),2))],...
            ['load4-3: r=',num2str(round(r3(2),2)), ', p=',num2str(round(p3(2),2))]}, 'location', 'SouthWest'); legend('boxoff');
        xlabel('Load 1 Drift'); ylabel('Change drift (see label)')
    subplot(1,3,3); hold on;
        l1 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), 'filled'); lsline();
        [r1, p1] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1));
        l2 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), 'filled'); lsline();
        [r2, p2] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2));
        l3 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,3), 'filled'); lsline();
        [r3, p3] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,2), HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,3));
        legend([l1,l2,l3],{['load4-1: r=',num2str(round(r1(2),2)), ', p=',num2str(round(p1(2),2))],...
            ['load4-2: r=',num2str(round(r2(2),2)), ', p=',num2str(round(p2(2),2))],...
            ['load4-3: r=',num2str(round(r3(2),2)), ', p=',num2str(round(p3(2),2))]}, 'location', 'SouthWest'); legend('boxoff');
        xlabel('Load 2 Drift'); ylabel('Change drift (see label)')
    set(findall(gcf,'-property','FontSize'),'FontSize',15)

    
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v2/';
    load([pn.out, 'GrandAverages_v2.mat'], 'dataResponseGrandAvg')
    
    %% plot median split of CPP based on HDDM-derived drift

    pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
    
    [~, sortInd] = sort(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1), 'descend');
    
    % add within-subject error bars, do stats between fast and slow performers
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')

    cBrew = brewermap(4,'RdBu');
    cBrew = flipud(cBrew);
    
    h = figure('units','normalized','position',[.1 .1 .4 .4]);

    % between-subject error bars
   
    cla;

    curData = squeeze(dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:), 'LineStyle', '-','linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);

    curData = squeeze(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:), 'LineStyle', '--','linewidth', 2}, 'patchSaturation', .1);
    
%     hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
%     hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
%     hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:),1)), 'LineWidth', 2)
%     hold on; plot(dataResponseGrandAvg{1,1}.time, squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:),1)), 'LineWidth', 2)
    xlim([-1000 100])
    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    legend([l1.mainLine, l3.mainLine, l2.mainLine, l4.mainLine],...
        {'Performers high drift Load 1'; 'Performers high drift Load 4'; 'Performers slow drift Load 1'; 'Performers low drift Load 4'}, 'location', 'NorthWest'); legend('boxoff');
    title('CPP median split according to high/low DDM drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',22)
    xlabel('Time (ms, response-locked)'); ylabel('ERP Amplitude @POz (µVolts)');
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/'; mkdir(pn.plotFolder);
    figureName = 'E_MedianSplitFastSlow';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% between-subject group t-tests good vs. bad performers
    
    % create different structures for the two groups
    
    idx_performance{1} = sortInd(1:ceil(numel(sortInd)/2));
    idx_performance{2} = sortInd(ceil(numel(sortInd)/2)+1:end);
    for indGroup = 1:2
        StatStruct{1,indGroup} = dataResponseGrandAvg{1,1};
        StatStruct{1,indGroup}.powspctrm = dataResponseGrandAvg{1,1}.individual(idx_performance{indGroup},54,:);
        StatStruct{1,indGroup}.label = [];
        StatStruct{1,indGroup}.label{1} = 'CPP';
        %StatStruct{1,indGroup}.dimord = 'subj_chan_time';
        StatStruct{1,indGroup}= rmfield(StatStruct{1,indGroup}, 'individual');
        StatStruct{2,indGroup} = dataResponseGrandAvg{4,1};
        StatStruct{2,indGroup}.powspctrm = dataResponseGrandAvg{4,1}.individual(idx_performance{indGroup},54,:);
        %StatStruct{2,indGroup}.dimord = 'subj_time';
        StatStruct{2,indGroup}= rmfield(StatStruct{2,indGroup}, 'individual');
        StatStruct{2,indGroup}.label = [];
        StatStruct{2,indGroup}.label{1} = 'CPP';
    end

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_indepsamplesT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.minnbchan        = 2;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 500;
    cfgStat.parameter        = 'powspctrm';
    cfgStat.neighbours       = [];

    N_1 = size(StatStruct{1,1}.powspctrm,1);
    N_2 = size(StatStruct{1,2}.powspctrm,1);
    design = zeros(2,N_1+N_2);
    design(1,1:N_1) = 1;
    design(1,N_1+1:end) = 2;

    cfgStat.design   = design;
    cfgStat.ivar     = 1;

    [stat] = ft_timelockstatistics(cfgStat, StatStruct{1,1}, StatStruct{1,2});

    save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')

    figure; imagesc(stat.time,[],stat.mask.*stat.stat)
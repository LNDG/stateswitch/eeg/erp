%% plot CPP slope - HDDM drift correlations

    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
    pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';

    addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
    ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

    load([pn.out, 'GrandAverages_v3.mat'], 'dataStimGrandAvg', 'dataProbeGrandAvg', 'dataResponseGrandAvg')

 %% individual threshold fits (CPP-based)
    
    % slope fit from -50 to 50 ms
    
    dataForSlopeFit = []; slopes = [];
    for indAge = 1:2
        for indCond = 1:4
            dataForSlopeFit{indAge}(:,indCond,:) = squeeze(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<-100));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                %slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))',1);
                slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
                %scatter(1:size(dataForSlopeFit{indAge},3), zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))', 'filled'); pause(.2)
            end
        end
    end
    
    %% test for threshold differences at CPP maximum

    for indAge = 1:2
        for indCond = 1:4
            curTime = dataResponseGrandAvg{indCond,indAge}.time;
            dataForThreshold{indAge}(:,indCond,:) = squeeze(nanmean(nanmean(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-50 & dataResponseGrandAvg{1,1}.time<50),2),3));
        end
    end

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
        condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
        condPairsLevel = [9*10^-4 9.5*10^-4 10*10^-4 10.5*10^-4 11*10^-4 11.5*10^-4];
        colorm = [.6 .6 .6];
        addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))
        hold on;
            dat = squeeze(dataForThreshold{1}(:,:));
            bar(1:4, nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
            % show standard deviation on top
            h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
            set(h1(1), 'marker', 'none'); % remove marker
            set(h1(2), 'LineWidth', 4);
            set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
                'xlim', [0.5 4.5]); ylim([0 1*10^-3])
            ylabel('CPP: -50:50 ms [�Volt]'); xlabel('# of targets');
            for indPair = 1:size(condPairs,1)
                % significance star for the difference
                [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
                % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
                % sigstars on top
                if pval <.05
                    mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
                end
            end
            title({'CPP threshold is not modulated by load'; ''})

    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/';
    figureName = 'E5_CPP_threshold';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    
    %% save threshold values
    
    CPPthreshold.data = dataForThreshold{1};
    CPPthreshold.IDs = IDs(ageIdx{1});

    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/Z_CPPthreshold_YA.mat'], 'CPPthreshold');
    
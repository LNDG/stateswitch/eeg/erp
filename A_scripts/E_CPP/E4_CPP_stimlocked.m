
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';
load([pn.out, 'GrandAverages_v3.mat'], 'dataStimGrandAvg')

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

h = figure('units','normalized','position',[.1 .1 .75 .35]);
set(0, 'DefaultFigureRenderer', 'painters');
subplot(1,3,1)
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.zlim = [-6*10^-4 6*10^-4];
    cfg.colormap = cBrew;
    cfg.marker = 'off';
    cfg.highlight = 'on';
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 50;
    cfg.highlightchannel = dataStimGrandAvg{1}.label{54};
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    grandAvg = cat(4, dataStimGrandAvg{1,1}.individual, dataStimGrandAvg{2,1}.individual, ...
        dataStimGrandAvg{3,1}.individual, dataStimGrandAvg{4,1}.individual);
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(grandAvg(:,:,idxTime,:),3),1),4))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude (?V)');
    title({'ERP during dynamic stimulus presentation'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
    figureName = 'E1_CPP_YA_StimPresentation_p1';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
h = figure('units','normalized','position',[.1 .1 .75 .35]);
set(0, 'DefaultFigureRenderer', 'painters');

subplot(1,3,[2,3]); cla; hold on;
    
    % highlight different experimental phases in background
    patches.timeVec = [3 6.066]-3;
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-1 1];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    
    time = dataStimGrandAvg{1,1}.time;
    time = time./1000;

    cBrew = brewermap(5,'greys');
    cBrew = cBrew(2:end,:);

    catAll = cat(4,dataStimGrandAvg{1,1}.individual,...
    dataStimGrandAvg{2,1}.individual,dataStimGrandAvg{3,1}.individual,...
    dataStimGrandAvg{4,1}.individual);

% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);

%     ll  = line([0 0], [ -8*10^-4 8*10^-4], 'LineStyle', '--', 'LineWidth', 2, 'Color', 'k');
%     ll  = line([3 3], [ -8*10^-4 8*10^-4], 'LineStyle', '--', 'LineWidth', 2, 'Color', 'k');
    %set(ll, 'Color', 'k')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
            {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', ...
            'South', 'orientation', 'horizontal'); legend('boxoff');
    xlim([-.5 4]); ylim([-8 8]*10^-4)
    xlabel('Time (ms from stimulus onset); response-locked'); ylabel('ERP Amplitude @POz (?V)');
    title({'CPP does not increase during stimulus processing'})

    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
    figureName = 'E1_CPP_YA_StimPresentation_p2';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
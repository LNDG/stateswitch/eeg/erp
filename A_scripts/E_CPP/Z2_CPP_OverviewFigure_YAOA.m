
% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))

pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';
load([pn.out, 'GrandAverages_v3.mat'], 'dataResponseGrandAvg')

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

h = figure('units','normalized','position',[.1 .1 .65 .6]);

subplot(2,3,1)
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.zlim = [-3*10^-4 6*10^-4];
    cfg.colormap = cBrew;
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<100;
    grandAvg = cat(4, dataResponseGrandAvg{1,1}.individual, dataResponseGrandAvg{2,1}.individual, ...
        dataResponseGrandAvg{3,1}.individual, dataResponseGrandAvg{4,1}.individual);
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(grandAvg(:,:,idxTime,:),3),1),4))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','ERP amplitude (?V)');
    title({'ERP (across loads): -250:100 ms peri-response'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
subplot(2,3,[2,3]); cla; hold on;
    
    cBrew = brewermap(4,'RdBu');

    catAll = cat(4,dataResponseGrandAvg{1,1}.individual,...
    dataResponseGrandAvg{2,1}.individual,dataResponseGrandAvg{3,1}.individual,...
    dataResponseGrandAvg{4,1}.individual);

% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);

    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
            {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');
    title({'CPP slope (@ POz) is parametrically modulated by load in YAs'; ''})

    %% plot correlation between HDDM- % CPP-derived drift estimates

subplot(2,3,4)

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_v12_a2_t2.mat')

    idx_YA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{1}));
    idx_OA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{2}));

    % slope fit from -250 to -100 (McGovern et al., 2018)
    
    dataForSlopeFit = []; slopes = [];
    for indAge = 1:2
        for indCond = 1:4
            dataForSlopeFit{indAge}(:,indCond,:) = squeeze(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<-100));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                %slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))',1);
                slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
                %scatter(1:size(dataForSlopeFit{indAge},3), zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))', 'filled'); pause(.2)
            end
        end
    end
    hold on;
    a = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
    b = squeeze(slopes{1}(:,1,1));
    l1 = scatter(a,b, 70,'k', 'filled'); l2 = polyval(polyfit(a,b,1),a); l2 = plot(a, l2,'Color', 'k', 'LineWidth', 3);
    [r1, p1] = corrcoef(a,b);
    pval1 = []; pval1 = convertPtoExponential(p1(2));

    a = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
    b = squeeze(slopes{2}(:,1,1));
    l3 = scatter(a,b, 70,'r', 'filled'); l4 = polyval(polyfit(a,b,1),a); l4 = plot(a, l4,'Color', 'r', 'LineWidth', 3);
    [r3, p3] = corrcoef(a,b);
    pval3 = []; pval3 = convertPtoExponential(p3(2));
        
    legend([l2, l4], ['YA: r = ', num2str(round(r1(2),2)), ' p = ', pval1{1}],...
        ['OA: r = ', num2str(round(r3(2),2)), ' p = ', pval3{1}],...
        'location', 'NorthWest'); legend('boxoff')
    title({'Relation between HDDM- & CPP-indicated drift rate'; ''})
    xlabel('HDDM drift (EEG) Load 1'); ylabel('CPP slope (EEG) Load 1'); 
    
    ylim([-.5*10^-5 3*10^-5])

    
%% plot median split of CPP based on HDDM-derived drift

    pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
    
    [~, sortInd] = sort(nanmean(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,:),2), 'descend');
    
    % add within-subject error bars, do stats between fast and slow performers
    
    subplot(2,3,[5,6]); cla;
    
%     % between-subject error bars
%    
%     curData = squeeze(dataResponseGrandAvg{1,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .4 .4],'linewidth', 2}, 'patchSaturation', .1);
% 
%     curData = squeeze(dataResponseGrandAvg{1,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l2 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', .6*[1 .4 .4],'linewidth', 2}, 'patchSaturation', .1);
% 
%     curData = squeeze(dataResponseGrandAvg{4,1}.individual(sortInd(1:ceil(numel(sortInd)/2)),54,:));
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l3 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', [.1 .7 1],'linewidth', 2}, 'patchSaturation', .1);
% 
%     curData = squeeze(dataResponseGrandAvg{4,1}.individual(sortInd(ceil(numel(sortInd)/2)+1:end),54,:));
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l4 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', .6*[.1 .7 1],'linewidth', 2}, 'patchSaturation', .1);
%     
%     xlim([-1000 100])
%     ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
%     set(ll, 'Color', 'k')
%     legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
%         {'Fast Performers Load 1'; 'Slow Performers Load 1'; 'Fast Performers Load 4'; 'Slow Performers Load 4'}, 'location', 'NorthWest'); legend('boxoff');
%     title({'CPP median split according to high/low HDDM drift across loads'; ''})
%     set(findall(gcf,'-property','FontSize'),'FontSize',22)
%     xlabel('Time (ms, response-locked)'); ylabel('ERP Amplitude @POz (?V)');
    
    cBrew = brewermap(4,'RdBu');

    catAll = cat(4,dataResponseGrandAvg{1,2}.individual,...
    dataResponseGrandAvg{2,2}.individual,dataResponseGrandAvg{3,2}.individual,...
    dataResponseGrandAvg{4,2}.individual);

% new value = old value ? subject average + grand average

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataResponseGrandAvg{1,2}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataResponseGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);

    ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
    set(ll, 'Color', 'k')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
            {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');
    title({'CPP slope (@ POz) and threshold are modulated by load in OAs'; ''})

    set(findall(gcf,'-property','FontSize'),'FontSize',18)
      
    %% add as insets: bar plots of slope decrease with load
    
    dataForSlopeFit = []; slopes = [];
    for indAge = 1:2
        for indCond = 1:4
            dataForSlopeFit{indAge}(:,indCond,:) = squeeze(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<-50));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
            end
        end
    end
    
    condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
    condPairsLevel = [7.3*10^-6 8.25*10^-6 7.75*10^-6 8.25*10^-6 5*10^-6 9.75*10^-6];
    
    colorm = [1 0 0; 1 .3 .3];
    
    %subplot(2,3,[2,3]); 
    axes('Position',[.53 .8 .1 .1])
    box off
    
%     figure; 
%     subplot(1,2,1); 
    hold on;
        dat = squeeze(slopes{1}(:,:,1));
        bar(1:4, nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % show standard deviation on top
        h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
        set(h1(1), 'marker', 'none'); % remove marker
        set(h1(2), 'LineWidth', 4);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
            'xlim', [0.5 4.5]); ylim([-1*10^-6 10*10^-6])
        ylabel('CPP slope'); xlabel('# of targets');
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
                mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
            end
        end
        %title('Younger Adults')
        
    axes('Position',[.53 .31 .1 .1])
    box off
    hold on;
    %subplot(1,2,2); hold on;
        dat = squeeze(slopes{2}(:,:,1));
        bar(1:4, nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % show standard deviation on top
        h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
        set(h1(1), 'marker', 'none'); % remove marker
        set(h1(2), 'LineWidth', 4);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
            'xlim', [0.5 4.5]); ylim([-1*10^-6 10*10^-6])
        ylabel('CPP slope'); xlabel('# of targets');
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
                mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
            end
        end
        %title('Older Adults')
        
set(findall(gcf,'-property','FontSize'),'FontSize',19)
 
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
figureName = 'Z2_CPP_YAOA_Overview';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
  
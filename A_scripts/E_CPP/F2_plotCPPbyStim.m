%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
pn.out          = [pn.root, 'B_analyses/S1_ERPs/B_data/E2_CCP_v4/'];

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/brewermap/')

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)
    load([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
    ERPstruct.dataStimAvg(:,:,id) = dataStimAvg;
    ERPstruct.dataProbeAvg(:,:,id) = dataProbeAvg;
    ERPstruct.dataResponseAvg(:,:,id) = dataResponseAvg;
end

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1:2
    for indCond = 1:4
        for indStim = 1:4
            cfg.keepindividual = 'yes';
            dataStimGrandAvg{indCond,indStim,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,indStim,ageIdx{indAge}});
            dataProbeGrandAvg{indCond,indStim,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg{indCond,indStim,ageIdx{indAge}});
            dataResponseGrandAvg{indCond,indStim,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg{indCond,indStim,ageIdx{indAge}});
        end
    end
end

%% save grand averages

save([pn.out, 'dataStimGrandAvg_v4.mat'], 'dataStimGrandAvg')
save([pn.out, 'dataProbeGrandAvg_v4.mat'], 'dataProbeGrandAvg')
save([pn.out, 'dataResponseGrandAvg_v4.mat'], 'dataResponseGrandAvg')

%% load grand averages

load([pn.out, 'dataStimGrandAvg_v4.mat'], 'dataStimGrandAvg')
load([pn.out, 'dataProbeGrandAvg_v4.mat'], 'dataProbeGrandAvg')
load([pn.out, 'dataResponseGrandAvg_v4.mat'], 'dataResponseGrandAvg')

%%

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')

cBrew = brewermap(4,'RdBu');

% h = figure('units','normalized','position',[.1 .1 .4 .35]);
% for indStim = 1:4
%     subplot(2,2,indStim)
%     cla; hold on;
%     
%     cBrew = brewermap(4,'RdBu');
% 
%     catAll = cat(4,dataResponseGrandAvg{1,indStim,1}.individual,...
%     dataResponseGrandAvg{2,indStim,1}.individual,dataResponseGrandAvg{3,indStim,1}.individual,...
%     dataResponseGrandAvg{4,indStim,1}.individual);
% 
% % new value = old value ? subject average + grand average
% 
%     condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
%     curData = squeeze(catAll(:,54,:,1));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(dataResponseGrandAvg{1,indStim,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
%     
%     curData = squeeze(catAll(:,54,:,2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l2 = shadedErrorBar(dataResponseGrandAvg{1,indStim,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
%     
%     curData = squeeze(catAll(:,54,:,3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l3 = shadedErrorBar(dataResponseGrandAvg{1,indStim,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
%     
%     curData = squeeze(catAll(:,54,:,4));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l4 = shadedErrorBar(dataResponseGrandAvg{1,indStim,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
% 
%     ll  = line([ 0 -2*10^-4], [ -2*10^-4 10*10^-4], 'LineStyle', '--', 'LineWidth', 2);
%     set(ll, 'Color', 'k')
%     legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine],...
%             {'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'South', 'Orientation', 'horizontal'); legend('boxoff');
%     xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');
%     title({'CPP slope decreases with prior target uncertainty'; ''})
%     
% end

l1 = []; l2 = []; l3 = []; l4 = [];
h = figure('units','normalized','position',[.1 .1 .2 .35]);
set(gcf,'renderer','Painters')
for indStim = 1:4
    hold on;
    cBrew = brewermap(4,'RdBu');
    catAll = cat(4,dataResponseGrandAvg{1,indStim,1}.individual,...
    dataResponseGrandAvg{2,indStim,1}.individual,dataResponseGrandAvg{3,indStim,1}.individual,...
    dataResponseGrandAvg{4,indStim,1}.individual);

    condAvg = squeeze(nanmean(catAll(:,54,:,:),4));
    curData = squeeze(catAll(:,54,:,1));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1{indStim} = shadedErrorBar(dataResponseGrandAvg{1,indStim,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indStim,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(catAll(:,54,:,4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    l4{indStim} = shadedErrorBar(dataResponseGrandAvg{4,indStim,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indStim,:),'linewidth', 2, 'LineStyle', '--'}, 'patchSaturation', .25);
end
ll1  = line([ 0 0], [ -4*10^-4 12*10^-4], 'LineStyle', ':', 'LineWidth', 2);
ll2  = line([ -1000 100], [ 0 0], 'LineStyle', ':', 'LineWidth', 2);
set(ll1, 'Color', 'k')
set(ll2, 'Color', 'k')
ylim([-1.5*10^-4 12*10^-4])
title({'CPP slope as a function of stimulus and load'; ''})
legend([l1{1}.mainLine, l1{2}.mainLine, l1{3}.mainLine, l1{4}.mainLine],...
            {'Color'; 'Direction'; 'Size'; 'Luminance'}, 'location', 'NorthWest', 'Orientation', 'vertical'); legend('boxoff');
xlim([-1000 100]); xlabel('Time (ms); response-locked'); ylabel('ERP Amplitude @POz (?V)');  
    set(findall(gcf,'-property','FontSize'),'FontSize',20)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
figureName = 'F2_CPPbyStim';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% extract slopes and thresholds for each feature and their modulations

dataForSlopeFit = []; slopes = [];
for indAge = 1:2
    for indCond = 1:4
        for indStim = 1:4
            dataForSlopeFit{indAge}(:,indCond,indStim,:) = squeeze(dataResponseGrandAvg{indCond,indStim,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-250 & dataResponseGrandAvg{1,1}.time<-50));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                slopes{indAge}(indID,indCond,indStim,:) = polyfit(1:size(dataForSlopeFit{indAge},4),squeeze(dataForSlopeFit{indAge}(indID,indCond,indStim,:))',1);
            end
        end
    end
end
dataForThreshold = [];
for indAge = 1:2
    for indCond = 1:4
        for indStim = 1:4
            curTime = dataResponseGrandAvg{1,1,1}.time;
            dataForThreshold{indAge}(:,indCond,indStim) = squeeze(nanmean(nanmean(dataResponseGrandAvg{indCond,indStim,indAge}.individual(:,[54],curTime>-50 & curTime<50),2),3));
        end
    end
end

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel = [10.3*10^-6 10.9*10^-6 11.75*10^-6 11.3*10^-6 7*10^-6 12.5*10^-6];

addpath('/Volumes/Kosciessa/Tools/ploterr');
addpath('/Volumes/Kosciessa/Tools/mysigstar');

features = {'Color'; 'Direction'; 'Size'; 'Luminance'};

h = figure('units','normalized','position',[.1 .1 .4 .4]);
set(gcf,'renderer','Painters')
for indStim = 1:4
    subplot(2,2,indStim)
    hold on;
        dat = squeeze(slopes{1}(:,:,indStim,1));
        bar(1:4, nanmean(dat), 'FaceColor',  cBrew(4,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % show standard deviation on top
        h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
        set(h1(1), 'marker', 'none'); % remove marker
        set(h1(2), 'LineWidth', 4);
        set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
            'xlim', [0.5 4.5]); ylim([-1*10^-6 14.5*10^-6])
        ylabel('CPP slope'); xlabel('# of targets');
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
                mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
            end
        end
        title(features{indStim})
end
set(findall(gcf,'-property','FontSize'),'FontSize',23)


pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
figureName = 'F2_CPPbyStim_SlopeByLoad';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% calculate linear beta slopes for each feature, test vs. zero

for indFeature = 1:4
    data = slopes{1}(:,1:4,indFeature);
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects(:,indFeature) = b(2,:);
end

h = figure('units','normalized','position',[.1 .1 .4 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1); cla; hold on;
    condPairsLevel = [-3 -3 -3 -3]*10^-6;
    dat = LinearLoadEffects;
    bar(1:4, nanmean(dat), 'FaceColor', [0.0196, 0.4431, 0.6902], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color', 'Direction', 'Size', 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-3.1 0]*10^-6)
    ylabel({'CPP slope';'[linear load beta]'}); xlabel('Probed Attribute');
    for indPair = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:,indPair)); % paired t-test vs 0
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [indPair], condPairsLevel(indPair), pval);
        end
    end
    
% plot linear slopes also for threshold

for indFeature = 1:4
    data = dataForThreshold{1}(:,1:4,indFeature);
    X = [1 1; 1 2; 1 3; 1 4]; b=X\data';
    LinearLoadEffects(:,indFeature) = b(2,:);
end

subplot(1,2,2); cla; hold on;
    condPairsLevel = [2.75, 2.75, 2.75, 2.75]*10^-4;

    dat = LinearLoadEffects;
    bar(1:4, nanmean(dat), 'FaceColor',  [.5 .5 .5], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color', 'Direction', 'Size', 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([-6 6]*10^-5)
    ylabel({'CPP threshold';'[linear load beta]'}); xlabel('Probed Attribute');
    for indPair = 1:4
        % significance star for the difference
        [~, pval] = ttest(dat(:,indPair)); % paired t-test vs 0
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [indPair], condPairsLevel(indPair), pval);
        end
    end
set(findall(gcf,'-property','FontSize'),'FontSize',20)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
figureName = 'F2_Slope_Thresh_Modulation_byProbe';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% do the same for threshold

% figure
% for indStim = 1:4
%     subplot(4,1,indStim)
%     hold on;
%         dat = squeeze(dataForThreshold{indAge}(:,:,indStim));
%         bar(1:4, nanmean(dat), 'FaceColor',  cBrew(4,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
%         % show standard deviation on top
%         h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
%         set(h1(1), 'marker', 'none'); % remove marker
%         set(h1(2), 'LineWidth', 4);
% %         set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
% %             'xlim', [0.5 4.5]); ylim([-1*10^-6 10.5*10^-6])
%         ylabel('CPP slope'); xlabel('# of targets');
%         for indPair = 1:size(condPairs,1)
%             % significance star for the difference
%             [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
%             % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%             % sigstars on top
%             if pval <.05
%                 mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%             end
%         end
% end



%% plot differences between stimuli

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel = [10*10^-6 10.2*10^-6 8.75*10^-6 9.3*10^-6 9*10^-6 9.5*10^-6];

h = figure('units','normalized','position',[.1 .1 .4 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1); hold on;
    dat = squeeze(nanmean(slopes{1}(:,1,:,1),2));
    bar(1:4, nanmean(dat), 'FaceColor',  [.2 .6 .2], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color'; 'Direction'; 'Size'; 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([0 10.5*10^-6])
    ylabel('CPP slope'); xlabel('Probed Feature');
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel = [8.3*10^-4 8.9*10^-4 7.75*10^-4 8.3*10^-4 8.5*10^-4 8.5*10^-4];

subplot(1,2,2); cla; hold on;
    dat = squeeze(nanmean(dataForThreshold{indAge}(:,1,:),2));
    bar(1:4, nanmean(dat), 'FaceColor',  [.2 .6 .2], 'EdgeColor', 'none', 'BarWidth', 0.8);
    % show standard deviation on top
    h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
    set(h1(1), 'marker', 'none'); % remove marker
    set(h1(2), 'LineWidth', 4);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'Color'; 'Direction'; 'Size'; 'Luminance'}, ...
        'xlim', [0.5 4.5]); ylim([0 9*10^-4])
    ylabel('CPP Threshold (?V)'); xlabel('Probed Feature');
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
set(findall(gcf,'-property','FontSize'),'FontSize',23)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E/';
figureName = 'F2_CPPbyStim_ThreshSlope';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


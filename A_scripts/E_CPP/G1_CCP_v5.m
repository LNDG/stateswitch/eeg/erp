% Calculate ERPs across subjects by Run of the STSW task.

% v2: different baselining procedure: 
%       250 ms before STIM for STIM
%       250 ms before PROBE for PROBE
%       250 ms before PROBE for RESP

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)

    display(['processing ID ' IDs{id}]);

    %% load data

    load([pn.dataIn, IDs{id}, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

    %% save TrlInfo from FT
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    %% CSD transform (compute prior to averaging)
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    
    %% 8 Hz low-pass filter
    
    cfg = [];
    cfg.lpfilter = 'yes';
    cfg.lpfreq = 8;
    cfg.lpfiltord = 6;
    cfg.lpfilttype = 'but';
    cfg.lpfiltdir = 'twopass';
    
    data = ft_preprocessing(cfg,data);
    
    %% re-segment data with respect to stim onset, probe onset & response 
    % note that ft_redefinetrial unfortunately doesn't work due to the
    % time-locked structure
    
    % stim onset
    
    timepoint = repmat(3, size(data.trial,1),1);
    time = repmat(data.time{1}, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+2000;
    dataStim = ft_redefinetrial(cfg, data);
    
    % baseline: average of -250 to 0 ms prior to stim onset
    
    cfg = [];
    cfg.baseline = [2.75 3];
    [dataStim] = ft_timelockbaseline(cfg, dataStim);
    dataStim.time = -500*2:2:2000*2; % correct timing
    
    %% baseline: average of -250 to 0 ms prior to probe onset
    
    cfg = [];
    cfg.baseline = [5.75 6];
    [data] = ft_timelockbaseline(cfg, data);
    
    % probe onset
    
    timepoint = repmat(6.04, size(data.trial,1),size(data.time,2));
    time = repmat(data.time, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+500;
    dataProbe = data;
    dataProbe.trial = NaN(size(data.trial,1),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,1)
        dataProbe.trial(indTrial,:,:) = data.trial(indTrial,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataProbe.time = -500*2:2:500*2;
    
    % response
    
    timepoint = repmat(6.04+TrlInfo(:,9), 1,size(data.time,2));
    time = repmat(data.time, size(data.trial,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-500;
    cfg.endsample = idx+500;
    trialToRemove = find(cfg.begsample<=0 | cfg.endsample>size(data.trial,3));
    dataResponse = data;
    dataResponse.trial = NaN(size(data.trial,1),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,1)
        if ismember(indTrial, trialToRemove)
            dataResponse.trial(indTrial,:,:) = NaN;
        else
        	dataResponse.trial(indTrial,:,:) = data.trial(indTrial,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
        end
    end
    dataResponse.time = -500*2:2:500*2;
    
    % remove empty trials from all sturctures
    
    dataStim.trial(trialToRemove,:,:) = [];
    dataProbe.trial(trialToRemove,:,:) = [];
    dataResponse.trial(trialToRemove,:,:) = [];
    TrlInfo(trialToRemove,:) = [];
        
%% timelock to different events
    
    for indCond = 1:4
        for indCorrect = 1:2
            cfg = [];
            cfg.trials = TrlInfo(:,8)==indCond & TrlInfo(:,10) == indCorrect-1;
            % timelock to stim onset
            dataStimAvg{indCond,indCorrect} = ft_timelockanalysis(cfg, dataStim);
            % timelock to probe onset
            dataProbeAvg{indCond,indCorrect} = ft_timelockanalysis(cfg, dataProbe);
            % timelock to response
            dataResponseAvg{indCond,indCorrect} = ft_timelockanalysis(cfg, dataResponse);
        end
    end
    
    %% save computed files

    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v5/'; mkdir(pn.out)
    save([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg', 'dataProbeAvg', 'dataResponseAvg');
    
end % ID

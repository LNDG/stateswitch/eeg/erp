% E6: plot putative motor preparation signal

%% load grand average ERPs

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/';
    addpath(genpath([pn.root, 'T_tools/RainCloudPlots/']));
    addpath(genpath([pn.root, 'T_tools/']));

    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';
    load([pn.root, 'B_data/E2_CCP_v3/GrandAverages_v3.mat'], 'dataResponseGrandAvg')

%% extract data across channels
    
    DataMat = [];
     for indCond = 1:4
        for indGroup = 1:2
            for indID = 1:size(dataResponseGrandAvg{indGroup}.individual,1)
                DataMat(indGroup,indCond, indID,:,:) = squeeze(nanmean(dataResponseGrandAvg{indCond,indGroup}.individual(indID,:,:),1));
            end
        end
     end
     
%% plot data averaged within frontal effects cluster

% add within-subject error bars
pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);

frontalCluster = [4,5,9:14,19:22];

% cBrew = brewermap(6,'Reds');
% cBrew = cBrew(3:6,:);

cBrew = [8.*[.12 .1 .1]; 6.*[.15 .1 .1]; 4.*[.2 .1 .1]; 2.*[.3 .1 .1]];

h = figure('units','normalized','position',[.1 .1 .4 .35]);
set(gcf,'renderer','Painters')

%subplot(1,3,[1,2]); 
cla; hold on;
    % new value = old value � subject average + grand average
    time = dataResponseGrandAvg{1,1}.time;
    data = squeeze(nanmean(DataMat(1,:,:,frontalCluster,:),4));
    condAvg = squeeze(nanmean(data,1));
    curData = squeeze(data(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(data(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(data(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(data(4,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .1);
%     plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,1,:,frontalCluster,:),4),3)), 'LineWidth', 2)
%     plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,2,:,frontalCluster,:),4),3)), 'LineWidth', 2)
%     plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,3,:,frontalCluster,:),4),3)), 'LineWidth', 2)
%     plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,4,:,frontalCluster,:),4),3)), 'LineWidth', 2)
    xlabel('Time (ms); response-locked')
    line([0 0],[-2*10^-4 2.2*10^-4], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'horizontal', 'location', 'SouthWest'); legend('boxoff');
    title({'Frontal increase may indicate additional motor preparation demands'; ''})
    xlim([-1000 50]); ylim([-1*10^-4 2.2*10^-4])
    ylabel('Frontal ERP amplitude (�V)')    
    
    %% Inset 1: plot bar with significance indication for lateralized beta

% %     condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
% %     condPairsLevel = [1*10^-4 1.1*10^-4 1.2*10^-4 1.3*10^-4 1.4*10^-4 1.5*10^-4];
%     
%     condPairs = [1,2; 2,3; 3,4];
%     condPairsLevel = [1.1*10^-4 1.3*10^-4 1.5*10^-4];
%     
%     colorm = [1 0 0; 1 .3 .3];
%     
%     addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))
%     
%  %   subplot(1,3,[3]); 
%     ax1 = axes('Position',[.4 .65 .17 .17]);
%     box off;
%     hold on;
%     
%         dat = squeeze(nanmean(data(:,:,time>-100 & time<0),3))';
%         bar(1:4, nanmean(dat), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
%         % show standard deviation on top
%         h1 = ploterr(1:size(dat,2), nanmean(dat,1), [], nanstd(dat,[],1)./sqrt(size(dat,1)), 'k.', 'abshhxy', 0);
%         set(h1(1), 'marker', 'none'); % remove marker
%         set(h1(2), 'LineWidth', 4);
%         set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
%             'xlim', [0.5 4.5]); ylim([-1*10^-4 1.6*10^-4])
%         ylabel({'Frontal ERP';'(�V)'}); xlabel('# of targets');
%         for indPair = 1:size(condPairs,1)
%             % significance star for the difference
%             [~, pval] = ttest(dat(:,condPairs(indPair,1)), dat(:,condPairs(indPair,2))); % paired t-test
%             % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%             % sigstars on top
%             if pval <.05
%                 mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%             end
%         end
%     set(findall(ax1,'-property','FontSize'),'FontSize',18)

    %% insert 1: plot RainCloudPlots
 
    data = squeeze(nanmean(DataMat(1,:,:,frontalCluster,:),4));
    curData = squeeze(nanmean(data(:,:,time>-100 & time<0),3))';
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = cBrew(4,:);

    axes('Position',[.3 .55 .2 .25])
    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        condPairs = [1,2; 2,3; 3,4];
        condPairsLevel = [4*10^-4 5*10^-4 6*10^-4];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel({'Frontal ERP';'(�V)'})
    %title('1/f slope modulation'); 
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim([-5*10^-4 6*10^-4]); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);

    %% save Figure
    set(findall(gcf,'-property','FontSize'),'FontSize',23)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/';
    figureName = 'E6_motorPreparationERP';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    %% save estimates
    
    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
    
    NDTpotential.data = curData;
    NDTpotential.IDs = IDs;

    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/Z_NDTpotential_YA.mat'], 'NDTpotential');

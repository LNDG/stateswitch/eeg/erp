%% load grand averages

    restoredefaultpath
    addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']); ft_defaults;

    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';
    load([pn.out, 'GrandAverages_v3.mat'], 'dataResponseGrandAvg')

    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
    
    %% calculate linear changes in ERP

    ERPresponses = cat(4, dataResponseGrandAvg{1,1}.individual, dataResponseGrandAvg{2,1}.individual,...
        dataResponseGrandAvg{3,1}.individual, dataResponseGrandAvg{4,1}.individual);
    
    for indID = 1:size(ERPresponses,1)
        for indChan = 1:size(ERPresponses,2)
            for indTime = 1:size(ERPresponses,3)
                X = [1 1; 1 2; 1 3; 1 4];
                b=X\squeeze(ERPresponses(indID, indChan, indTime, :));
                ERPresponses_lin(indID, indChan, indTime, :) = b(2,:);
            end
        end
    end

    CBPAstruct = [];
    indGroup = 1;
    CBPAstruct = dataResponseGrandAvg{indCond,indGroup};
    CBPAstruct.dimord = 'subj_chan_time';
    CBPAstruct.individual = ERPresponses_lin;

%% load summary data

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')
    tmp_IDidx = ismember(STSWD_summary.IDs, IDs);
    
%% CBPA of pre-response potential: relation of linear changes to linear changes in NDT
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = elec.label;

    cfgStat = [];
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_indepsamplesregrT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    cfgStat.minnbchan        = 2;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.025;
    cfgStat.numrandomization = 500;
    cfgStat.parameter        = 'individual';
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb,CBPAstruct);

    subj = size(CBPAstruct.individual,1);
    cfgStat.design(1,1:subj)= STSWD_summary.HDDM_vt.nondecisionEEG_linear(tmp_IDidx, 1);
    cfgStat.ivar     = 1;

    [stat] = ft_timelockstatistics(cfgStat, CBPAstruct);

    figure; imagesc(stat.stat)
%% load grand averages

    restoredefaultpath
    addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']); ft_defaults;

    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';
    load([pn.out, 'GrandAverages_v3.mat'], 'dataResponseGrandAvg')

    %% build stats matrices
    
    CBPAstruct = [];
    indGroup = 1;
    for indCond = 1:4
        CBPAstruct{indCond} = dataResponseGrandAvg{indCond,indGroup};
        CBPAstruct{indCond}.dimord = 'subj_chan_time';
        CBPAstruct{indCond}.time = CBPAstruct{indCond}.time;
        CBPAstruct{indCond}.individual = CBPAstruct{indCond}.individual;
    end
    
%% CBPA of pre-response potential: linear loadings

    cfg = [];
    cfg.method           = 'montecarlo';
    cfg.statistic        = 'ft_statfun_depsamplesregrT';
    cfg.latency          = [-200 0];
    cfg.avgovertime      = 'yes';
    cfg.correctm         = 'cluster';
    cfg.clusteralpha     = 0.05;
    cfg.clusterstatistic = 'maxsum';
    %cfg.minnbchan        = 2;
    cfg.tail             = 1;
    cfg.clustertail      = 1;
    cfg.alpha            = 0.05;
    cfg.numrandomization = 1000; % number of iterations

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = CBPAstruct234.label;
    cfg.neighbours          = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1}); % use original dataset
    clear cfg_neighb;

    % specify design
    subj = size(CBPAstruct{1}.individual,1);
    conds = 4;
    design = zeros(2,conds*subj);
    for i = 1:subj
        for icond = 1:conds
            design(1,(icond-1)*subj+i) = i;
        end
    end
    for icond = 1:conds
        design(2,(icond-1)*subj+1:icond*subj) = icond;
    end

    cfg.design = design; clear design subj;
    cfg.uvar  = 1;
    cfg.ivar  = 2;
    cfg.parameter = 'individual'; % specify the field in which relevant data is contained

    [stat] = ft_timelockstatistics(cfg, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});
    
     %% topography of effect
    
%     pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
%     pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
%     addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = [-6 6];

%     figure;
%     subplot(1,3,1);
%         cfg.highlight = 'on';
%         cfg.highlightchannel = find(stat_234.mask);
%         plotData = [];
%         plotData.label = stat_234.label; % {1 x N}
%         plotData.dimord = 'chan';
%         plotData.powspctrm = stat_234.stat;
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     subplot(1,3,2);
%         cfg.highlight = 'on';
%         cfg.highlightchannel = find(stat_1.mask);
%         plotData = [];
%         plotData.label = stat_1.label; % {1 x N}
%         plotData.dimord = 'chan';
%         plotData.powspctrm = stat_1.stat;
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     subplot(1,3,3);
%         cfg.highlight = 'on';
%         cfg.highlightchannel = find(stat.mask);
%         plotData = [];
%         plotData.label = stat.label; % {1 x N}
%         plotData.dimord = 'chan';
%         plotData.powspctrm = stat.stat;
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
h = figure('units','normalized','position',[.1 .1 .2 .2]);
    cfg.highlight = 'on';
    cfg.highlightchannel = find(stat.mask);
    plotData = [];
    plotData.label = stat.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = stat.stat;
    ft_topoplotER(cfg,plotData);
    title({'ERP amplitude increases in frontal cluster';'with load prior to response'})
    cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','t-values');
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    colormap(cBrew)
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/';
figureName = 'E5_CBPA_topography';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');

%% extract frontal channel cluster

figure; plot(stat.posclusterslabelmat) % frontal cluster = 1; posterior cluster = 2
figure; plot(stat.negclusterslabelmat) % frontal cluster = 1; posterior cluster = 2

find(stat.posclusterslabelmat==1)'

frontalCluster = [4,5,9:14,19:22];

frontalCluster = [1,2,4,5,9,10,11,12,13,14,19,20,21,22];
% Check out dynamics immediately prior to response

%% load grand averages

    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/E2_CCP_v3/';
    load([pn.out, 'GrandAverages_v3.mat'], 'dataResponseGrandAvg')

%% extract Cz channel 29

    DataMat = [];
     for indCond = 1:4
        for indGroup = 1:2
            for indID = 1:size(dataResponseGrandAvg{indGroup}.individual,1)
                DataMat(indGroup,indCond, indID,:) = squeeze(nanmean(nanmedian(dataResponseGrandAvg{indCond,indGroup}.individual(indID,[29],:),2),1));
            end
        end
     end
    
     %% load DDM estimates

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary.mat')

    DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)), cell2mat(cellfun(@str2num, IDs, 'un', 0)));
    DDMIDidx = find(DDMIDidx);

    %% event-lock to individual NDT in each condition

    ERPdata_NDTlocked = []; ERPdata_nonNDTlocked = [];
    for indGroup = 1
    for indID = 1:numel(IDs)
        for indDim = 1:4
           curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),indDim),1));
           curNDT1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),1),1));
           timeIdx = find(dataResponseGrandAvg{1,1}.time>-1000.*(curNDT-curNDT1),1,'first');
           timeIdx_nonNDT = find(dataResponseGrandAvg{1,1}.time>0,1,'first');
           ERPdata_NDTlocked(indGroup,indDim,indID,:) = DataMat(indGroup,indDim,indID,timeIdx-100:timeIdx+100);
           ERPdata_nonNDTlocked(indGroup,indDim,indID,:) = DataMat(indGroup,indDim,indID,timeIdx_nonNDT-100:timeIdx_nonNDT+100);
        end
    end
    end

     
        figure; hold on;
        plot([-100:100]*.004,squeeze(nanmean(ERPdata_nonNDTlocked(1,2,:,:)-ERPdata_nonNDTlocked(1,1,:,:),3)))
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_NDTlocked(1,2,:,:)-ERPdata_NDTlocked(1,1,:,:),3)))

        figure; hold on;
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_NDTlocked(1,1,:,:),3)))
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_NDTlocked(1,2,:,:),3)))
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_NDTlocked(1,3,:,:),3)))
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_NDTlocked(1,4,:,:),3)))

        figure; hold on;
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_nonNDTlocked(1,1,:,:),3)))
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_nonNDTlocked(1,2,:,:),3)))
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_nonNDTlocked(1,3,:,:),3)))
        plot([-100:100]*.004, squeeze(nanmean(ERPdata_nonNDTlocked(1,4,:,:),3)))

        
    figure; imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(DataMat(1,2,:,:)-DataMat(1,1,:,:)))
     
    h = figure('units','normalized','position',[.1 .1 .3 .3]);
    hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(1,1,:,:),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(1,4,:,:),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(DataMat(2,1,:,:),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(DataMat(2,4,:,:),3)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    legend({'YA: load1', 'YA: load4', 'OA: load1', 'OA: load4'}, 'location', 'NorthWest'); legend('boxoff');
    title('YA: Motor (@ Cz) relative to response')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/'; mkdir(pn.plotFolder);
    figureName = 'E4_PreResponseCz';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');


    % switch conditions have an additional peak in Cz prior to response (recall?)
    h = figure('units','normalized','position',[.1 .1 .3 .5]);
    subplot(2,1,1); hold on;
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(zscore(DataMat(1,4,:,:),[],4)-zscore(DataMat(1,1,:,:),[],4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(zscore(DataMat(1,3,:,:),[],4)-zscore(DataMat(1,1,:,:),[],4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(zscore(DataMat(1,2,:,:),[],4)-zscore(DataMat(1,1,:,:),[],4),3)), 'LineWidth', 2)
         xlim([-800 200]); ylim([-1 1]); xlabel('Time (ms); response-locked'); ylabel('Difference magnitude (z-score)')
         legend({'4-1', '3-1', '2-1'}, 'location', 'NorthWest'); legend('boxoff');
         title('YA: Cz relative to response')
    subplot(2,1,2); hold on;
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(zscore(DataMat(2,4,:,:),[],4)-zscore(DataMat(2,1,:,:),[],4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(zscore(DataMat(2,3,:,:),[],4)-zscore(DataMat(2,1,:,:),[],4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(zscore(DataMat(2,2,:,:),[],4)-zscore(DataMat(2,1,:,:),[],4),3)), 'LineWidth', 2)
        xlim([-800 200]); ylim([-1 1]); xlabel('Time (ms); response-locked'); ylabel('Difference magnitude (z-score)')
        legend({'4-1', '3-1', '2-1'}, 'location', 'NorthWest'); legend('boxoff');
        title('OA: Cz relative to response')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/'; mkdir(pn.plotFolder);
    figureName = 'E4_PreResponseCz_ZscoredDiff';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% extract data across channels
    
    DataMat = [];
     for indCond = 1:4
        for indGroup = 1:2
            for indID = 1:size(dataResponseGrandAvg{indGroup}.individual,1)
                DataMat(indGroup,indCond, indID,:,:) = squeeze(nanmean(dataResponseGrandAvg{indCond,indGroup}.individual(indID,:,:),1));
            end
        end
     end
    
    ERPdata_NDTlocked = []; ERPdata_nonNDTlocked = [];
    for indGroup = 1
        for indID = 1:numel(IDs)
            for indDim = 1:4
               curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),indDim),1));
               curNDT1 = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx(indID),1),1));
               timeIdx = find(dataResponseGrandAvg{1,1}.time>-1000.*(curNDT-curNDT1),1,'first');
               timeIdx_nonNDT = find(dataResponseGrandAvg{1,1}.time>0,1,'first');
               ERPdata_NDTlocked(indGroup,indDim,indID,:,:) = DataMat(indGroup,indDim,indID,:,timeIdx-100:timeIdx+100);
               ERPdata_nonNDTlocked(indGroup,indDim,indID,:,:) = DataMat(indGroup,indDim,indID,:,timeIdx_nonNDT-100:timeIdx_nonNDT+100);
            end
        end
    end
     
    idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'P3', 'P4', 'P5', 'P6', 'CP3', 'CP5', 'CP6', 'CP4'}));
    
%     h = figure('units','normalized','position',[.1 .1 .4 .4]); 
%     subplot(2,1,1); cla; hold on;
%         plot([-100:100]*.004,squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
%         plot([-100:100]*.004,squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
%         plot([-100:100]*.004,squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
%         plot([-100:100]*.004,squeeze(nanmean(nanmean(ERPdata_nonNDTlocked(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
%         xlabel('Time (ms); response-locked')
%         legend({'load 1'; 'load 2'; 'load 3'; 'load 4'}); legend('boxoff');
%         title('YA: Bilateral parietal relative to response')
    
     
    % bilateral parietal potential increases prior to stimulus onset in
    % switch condition, peaks right after response (but: accurate timing here?)
    
    idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'P3', 'P4', 'P5', 'P6', 'CP3', 'CP5', 'CP6', 'CP4'}));
    idx_Chan = [12,44,53];
    
    h = figure('units','normalized','position',[.1 .1 .4 .4]); 
    subplot(2,1,1); hold on;
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        xlabel('Time (ms); response-locked')
        legend({'load 1'; 'load 2'; 'load 3'; 'load 4'}); legend('boxoff');
        title('YA: Bilateral parietal relative to response')
    subplot(2,1,2); hold on;
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        xlabel('Time (ms); response-locked')
        legend({'load 1'; 'load 2'; 'load 3'; 'load 4'}); legend('boxoff');
        title('OA: Bilateral parietal relative to response')
     set(findall(gcf,'-property','FontSize'),'FontSize',14)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/'; mkdir(pn.plotFolder);
    figureName = 'E4_BilateralParietalPotential';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
        
    DataMat = [];
    for indCond = 1:4
        for indGroup = 1:2
            for indID = 1:numel(chanIdx{indGroup})
                DataMat(indGroup,indCond, indID,:,:) = squeeze(dataResponseGrandAvg{indCond,indGroup}.individual(indID,:,:));
            end
        end
    end
    
%     figure;
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(zscore(DataMat(1,4,:,:,:),[],5)-zscore(DataMat(1,1,:,:,:),[],5),3)))
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(zscore(DataMat(1,3,:,:,:),[],5)-zscore(DataMat(1,1,:,:,:),[],5),3)))
%         
  
    figure;
    DataMat_diff = diff(DataMat,1,5);
    imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat_diff(1,3,:,:,:)-DataMat_diff(1,1,:,:,:),3)))
    imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat_diff(1,3,:,:,:),3)))
    imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat_diff(1,1,:,:,:),3)))

    
    %% can difference signature account for NDT shift?
    
    idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'F2', 'F4', 'PO3', 'PO7'}));
    idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'C3', 'C1', 'Cz', 'C2', 'C4'}));
    idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'P3', 'P4', 'P5', 'P6', 'CP3', 'CP5', 'CP6', 'CP4'}));

    % bilateral parietal potential increases at when response is made
    %idx_Chan = [35, 36];
    %idx_Chan = [56];
    
    h = figure('units','normalized','position',[.1 .1 .3 .3]); 
    subplot(2,1,1); hold on;
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    subplot(2,1,2); hold on;
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        xlabel('Time (ms); response-locked')
        title('YA: XXX relative to response')
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary.mat')

   idx_YA = ismember(HDDM_summary.IDs, IDs(ageIdx{1}));
   idx_OA = ismember(HDDM_summary.IDs, IDs(ageIdx{2}));
   
   a = squeeze(nanmean(nanmean(DataMat(1,2,1:47,idx_Chan,time>-200&time<-100),5),4))-...
       squeeze(nanmean(nanmean(DataMat(1,1,1:47,idx_Chan,time>-200&time<-100),5),4));
   b = HDDM_summary.nondecisionEEG(idx_YA,2)-HDDM_summary.nondecisionEEG(idx_YA,1);
   
   figure; scatter(a,b, 'filled')
   
   figure; imagesc(squeeze(nanmean(DataMat(2,4,1:53,idx_Chan(3:4),:),4)))
   
    a = squeeze(nanmean(nanmean(DataMat(2,3,1:53,idx_Chan,time>-200&time<-100),5),4))-...
       squeeze(nanmean(nanmean(DataMat(2,1,1:53,idx_Chan,time>-200&time<-100),5),4));
   b = HDDM_summary.nondecisionEEG(idx_OA,3)-HDDM_summary.nondecisionEEG(idx_OA,1);
   
   figure; scatter(a,b, 'filled')

   figure;
   bar(nanmean(HDDM_summary.nondecisionEEG,1))
   scatter(HDDM_summary.nondecisionEEG(idx_YA,4), HDDM_summary.nondecisionEEG(idx_YA,4)-HDDM_summary.nondecisionEEG(idx_YA,1))
   
   NDTshift = HDDM_summary.nondecisionEEG(idx_YA,4)-HDDM_summary.nondecisionEEG(idx_YA,1);
   %NDTshift = HDDM_summary.nondecisionEEG(idx_YA,1);
   NDTshift = -1.*NDTshift.*1000;
   
   figure;
   [~, sortInd] = sort(NDTshift, 'ascend');
   chans = 10:12;
   imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(nanmean(DataMat(1,2:4,sortInd,[chans],:),4),2)-...
       nanmean(nanmean(DataMat(1,1,sortInd,[chans],:),4),2)))
   imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(nanmean(DataMat(1,2:4,sortInd,[chans],:),4),2)))
   imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(nanmean(DataMat(1,1,sortInd,[chans],:),4),2)))
        
    %% get estimates of motor 'NDT' as change from L1, find neural signatures
    
   load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary.mat')

   idx_YA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{1}));
   idx_OA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{2}));
   
   figure; scatter(HDDM_summary.nondecisionEEG(idx_YA,1),HDDM_summary.nondecisionEEG(idx_YA,4))

   NDTshift = HDDM_summary.nondecisionEEG(idx_YA,4)-HDDM_summary.nondecisionEEG(idx_YA,1);
   NDTshift = -1.*NDTshift.*1000;
   
   time = dataResponseGrandAvg{indCond,indGroup}.time;
   
   DataMat_alignedNDT = []; DataMat_NonAlignedNDT = [];
    for indCond = 1:4
        for indGroup = 1:2
            curIdx = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{indGroup}));
            if indCond == 1
                NDTshift = nanmean(HDDM_summary.nondecisionEEG(curIdx,2:4),2)-HDDM_summary.nondecisionEEG(curIdx,1);
                NDTshift(:) = 0;%-1.*NDTshift.*1000;
            else
               %NDTshift = nanmean(HDDM_summary.nondecisionEEG(curIdx,2:4),2)-HDDM_summary.nondecisionEEG(curIdx,1);
               NDTshift = HDDM_summary.nondecisionEEG(curIdx,indCond)-HDDM_summary.nondecisionEEG(curIdx,1);
               %NDTshift(:) = 0 
               NDTshift = -1.*NDTshift.*1000;
            end
            for indID = 1:numel(chanIdx{indGroup})
                [~, timeIdx] = min(abs(time-NDTshift(indID)));
                DataMat_alignedNDT(indGroup,indCond, indID,:,:) = squeeze(dataResponseGrandAvg{indCond,indGroup}.individual(indID,:,timeIdx-300:timeIdx+300));
                [~, timeIdx] = min(abs(time-0));
                DataMat_NonAlignedNDT(indGroup,indCond, indID,:,:) = squeeze(dataResponseGrandAvg{indCond,indGroup}.individual(indID,:,timeIdx-300:timeIdx+300));
            end
        end
    end
    
    idx_Chan = [35, 36];
    idx_Chan = [28:30];

    
    h = figure('units','normalized','position',[.1 .1 .3 .3]); 
    subplot(2,1,1); hold on;
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    subplot(2,1,2); hold on;
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
        xlabel('Time (ms); response-locked')
        title('YA: XXX relative to NDT')
        
    h = figure('units','normalized','position',[.1 .1 .3 .3]); 
    subplot(2,1,1); hold on;
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,2,:,29,:)-DataMat_alignedNDT(1,1,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,3,:,29,:)-DataMat_alignedNDT(1,1,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,4,:,29,:)-DataMat_alignedNDT(1,1,:,29,:),4),3)), 'LineWidth', 2)
        line([0 0], [-4*10^-4, 2*10^-4])
        xlabel('Time (ms); response-locked')
        title('YA: CPP relative to residual NDT')
    subplot(2,1,2); hold on;
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,2,:,29,:)-DataMat_alignedNDT(2,1,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,3,:,29,:)-DataMat_alignedNDT(2,1,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,4,:,29,:)-DataMat_alignedNDT(2,1,:,29,:),4),3)), 'LineWidth', 2)
        line([0 0], [-4*10^-4, 2*10^-4])
        xlabel('Time (ms); response-locked')
        title('OA: CPP relative to residual NDT')
        
    h = figure('units','normalized','position',[.1 .1 .3 .3]); 
    subplot(2,1,1); hold on;
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,1,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,3,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(1,4,:,29,:),4),3)), 'LineWidth', 2)
        line([0 0], [-4*10^-4, 2*10^-4])
        xlabel('Time (ms); response-locked')
        title('YA: CPP relative to residual NDT')
    subplot(2,1,2); hold on;
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,1,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,3,:,29,:),4),3)), 'LineWidth', 2)
        plot(-300:1:300,squeeze(nanmean(nanmean(DataMat_alignedNDT(2,4,:,29,:),4),3)), 'LineWidth', 2)
        line([0 0], [-4*10^-4, 2*10^-4])
        xlabel('Time (ms); response-locked')
        title('OA: CPP relative to residual NDT')
    
    DataMat_alignedNDT_z = zscore(DataMat_alignedNDT,[],5); 
   
   figure;
    imagesc(-300:1:300,[],squeeze(nanmean(zscore(DataMat_alignedNDT(1,4,:,:,:),[],5),3)))
    
    imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT(1,2:4,:,:,:),2)-DataMat_alignedNDT(1,1,:,:,:),3)))
    imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT(2,2:4,:,:,:),2)-DataMat_alignedNDT(2,1,:,:,:),3)))
    imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT(2,2:4,:,:,:),2),3)))
    imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT(2,1,:,:,:),2),3)))

    imagesc(-300:1:300,[],squeeze(nanmean(-1.*DataMat_NonAlignedNDT(1,1,:,:,:),3)))
    imagesc(-300:1:300,[],zscore(squeeze(nanmean(nanmean(DataMat_alignedNDT(1,2:4,:,:,:)-DataMat_NonAlignedNDT(1,2:4,:,:,:),2)--1.*DataMat_NonAlignedNDT(1,1,:,:,:),3)),[],2))
    imagesc(-300:1:300,[],squeeze(nanmean((DataMat_alignedNDT(1,3,:,:,:)-DataMat_NonAlignedNDT(1,3,:,:,:))--1.*DataMat_NonAlignedNDT(1,1,:,:,:),3)))
    imagesc(-300:1:300,[],squeeze(nanmean((DataMat_alignedNDT(1,2,:,:,:)-DataMat_NonAlignedNDT(1,2,:,:,:))--1.*DataMat_NonAlignedNDT(1,1,:,:,:),3)))

    imagesc(-300:1:300,[],squeeze(nanmean(DataMat_alignedNDT(1,2,:,:,:),3)))
    imagesc(-300:1:300,[],squeeze(nanmean(DataMat_alignedNDT(1,3,:,:,:),3)))
    imagesc(-300:1:300,[],squeeze(nanmean(zscore(DataMat_alignedNDT(1,4,:,:,:),[],5),3)))

    imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT_diff(2,2:4,:,:,:),3),2)))
    imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT_diff(1,2:4,:,:,:),3),2)))
        imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT(2,2:4,:,:,:),3),2)))
        imagesc(-300:1:300,[],squeeze(nanmean(nanmean(DataMat_alignedNDT(1,2:4,:,:,:),3),2)))

    h = figure('units','normalized','position',[.1 .1 .3 .3]);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    time = dataResponseGrandAvg{1,1}.time;
    tmpData = squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT_z(1,2:4,:,:,300:320),2)-...
        DataMat_alignedNDT_z(1,1,:,:,300:320),5),3))-...
        squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT_z(1,2:4,:,:,250:300),2)-...
        DataMat_alignedNDT_z(1,1,:,:,250:300),5),3));
    
    tmpData = squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT_z(2,2:4,:,:,300:320),2)-...
        DataMat_alignedNDT_z(2,1,:,:,300:320),5),3))-...
        squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT_z(2,2:4,:,:,250:300),2)-...
        DataMat_alignedNDT_z(2,1,:,:,250:300),5),3));
    
    tmpData = squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT_z(1,2:4,:,:,300:320),2)-...
        DataMat_alignedNDT_z(1,1,:,:,300:320),5),3));
    plotData.powspctrm = squeeze(tmpData);
    ft_topoplotER(cfg,plotData);
    
    subplot(2,2,1);
        tmpData = squeeze(nanmean(nanmedian(nanmedian(DataMat_alignedNDT(1,1,:,:,300:350),2),5)-...
             nanmedian(nanmedian(DataMat_alignedNDT(1,1,:,:,250:300),2),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    subplot(2,2,2);
        tmpData = squeeze(nanmean(nanmedian(nanmedian(DataMat_alignedNDT(1,2:4,:,:,300:350),2),5)-...
             nanmedian(nanmedian(DataMat_alignedNDT(1,2:4,:,:,250:300),2),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    subplot(2,2,3);
        tmpData = squeeze(nanmean(nanmedian(nanmedian(DataMat_alignedNDT(2,1,:,:,300:350),2),5)-...
             nanmedian(nanmedian(DataMat_alignedNDT(2,1,:,:,250:300),2),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    subplot(2,2,4);
        tmpData = squeeze(nanmean(nanmedian(nanmedian(DataMat_alignedNDT(2,2:4,:,:,300:350),2),5)-...
             nanmedian(nanmedian(DataMat_alignedNDT(2,2:4,:,:,250:300),2),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        
     tmpData = squeeze(nanmean(nanmean(DataMat_alignedNDT(1,1,:,:,300:320),5),3));
     tmpData = squeeze(nanmean(nanmean(DataMat_alignedNDT(1,4,:,:,300:320),5),3));
     tmpData = squeeze(nanmean(nanmean(DataMat_alignedNDT(1,4,:,:,280:300),5),3));
    plotData.powspctrm = squeeze(tmpData);
    ft_topoplotER(cfg,plotData);
    
    tmpData = squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT_z(2,2:4,:,:,300:320),2)-...
        DataMat_alignedNDT_z(2,1,:,:,300:320),5),3));
    plotData.powspctrm = squeeze(tmpData);
    ft_topoplotER(cfg,plotData);
        
        idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'F2', 'F4', 'PO3', 'PO7'}));
        
        figure; hold on;
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,2:4,:,idx_Chan,:),4),3),2)))
        plot(-299:1:300,squeeze(nanmean(nanmean(nanmean(diff(DataMat_alignedNDT(1,2:4,:,idx_Chan,:),1,5),4),3),2)))
        
                plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,4,:,29,:),4),3),2)))

        plot(-299:1:300,squeeze(nanmean(nanmean(nanmean(diff(DataMat_alignedNDT(1,1,:,29,:),1,5),4),3),2)))
        plot(-299:1:300,squeeze(nanmean(nanmean(nanmean(diff(DataMat_alignedNDT(1,2,:,29,:),1,5),4),3),2)))
        plot(-299:1:300,squeeze(nanmean(nanmean(nanmean(diff(DataMat_alignedNDT(1,3,:,29,:),1,5),4),3),2)))
        plot(-299:1:300,squeeze(nanmean(nanmean(nanmean(diff(DataMat_alignedNDT(1,4,:,29,:),1,5),4),3),2)))

        
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(2,1,:,idx_Chan,:),4),3),2)))
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,1,:,idx_Chan,:),4),3),2)))
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,2,:,idx_Chan,:),4),3),2)))
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,3,:,idx_Chan,:),4),3),2)))
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,4,:,idx_Chan,:),4),3),2)))
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(2,2,:,idx_Chan,:),4),3),2)))
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(2,3,:,idx_Chan,:),4),3),2)))
        plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(2,4,:,idx_Chan,:),4),3),2)))

    idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'C3', 'C1', 'Cz', 'C2', 'C4'}));
    idx_Chan = find(ismember(dataResponseGrandAvg{1,1}.label,{'Cz'}));

    figure; hold on;
    plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,2:4,:,idx_Chan,:),4),2),3)))
    plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(2,2:4,:,idx_Chan,:),4),2),3)))

    
    plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(zscore(DataMat_alignedNDT(2,1,:,idx_Chan,:),[],5),4)+...
        nanmean(zscore(DataMat_alignedNDT(2,1,:,idx_Chan2,:),[],5),4),3),2)))
    plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(zscore(DataMat_alignedNDT(2,1,:,idx_Chan,:),[],5),4),3),2)))

    
    plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,3,:,idx_Chan,:),4),3),2)))
    plot(-300:1:300,squeeze(nanmean(nanmean(nanmean(DataMat_alignedNDT(1,4,:,idx_Chan,:),4),3),2)))
        
    h = figure('units','normalized','position',[.1 .1 .3 .3]);
    hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(1,1,:,29,:)-nanmean(DataMat(1,1,:,28,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(1,2,:,29,:)-nanmean(DataMat(1,2,:,28,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(1,3,:,29,:)-nanmean(DataMat(1,3,:,28,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(1,4,:,29,:)-nanmean(DataMat(1,4,:,28,:),4),3)), 'LineWidth', 2)
    legend({'1', '2', '3', '4'});
    
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(2,1,:,29,:)-nanmean(DataMat(2,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(DataMat(2,4,:,29,:)-nanmean(DataMat(2,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)

    h = figure('units','normalized','position',[.1 .1 .3 .3]); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    legend({'YA: load1', 'YA: load4', 'OA: load1', 'OA: load4'}, 'location', 'NorthWest'); legend('boxoff');
    title('YA: XXX relative to response')
    
    h = figure('units','normalized','position',[.1 .1 .3 .3]); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,1,:,54,:)-DataMat(2,1,:,29,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,2,:,54,:)-DataMat(2,2,:,29,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,3,:,54,:)-DataMat(2,3,:,29,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(DataMat(2,4,:,54,:)-DataMat(2,4,:,29,:),4),3)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    legend({'YA: load1', 'YA: load4', 'OA: load1', 'OA: load4'}, 'location', 'NorthWest'); legend('boxoff');
    title('YA: XXX relative to response')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    figure;
    imagesc(dataResponseGrandAvg{1,2}.time,[],squeeze(nanmean(DataMat(2,1,:,54,:)-DataMat(2,1,:,29,:),4)))
    imagesc(dataResponseGrandAvg{1,2}.time,[],diff(squeeze(nanmean(DataMat(2,2,:,54,:)-DataMat(2,2,:,29,:),4)),1,2))
    imagesc(dataResponseGrandAvg{1,2}.time,[],diff(squeeze(nanmean(DataMat(2,1,:,54,:)-DataMat(2,1,:,29,:),4)),1,2))
    imagesc(dataResponseGrandAvg{1,2}.time,[],diff(squeeze(nanmean(DataMat(2,3,:,54,:)-DataMat(2,3,:,29,:),4)),1,2))

        imagesc(dataResponseGrandAvg{1,2}.time,[],diff(squeeze(nanmean(DataMat(2,4,:,54,:),4)),1,2))
        
        imagesc(dataResponseGrandAvg{1,2}.time,[],squeeze(nanmean(DataMat(2,4,:,54,:),4)))

        imagesc(dataResponseGrandAvg{1,2}.time,[],diff(squeeze(nanmean(DataMat(2,3,:,29,:),4)),1,2))
        
    % extract periods with low change & high change
    
    DataMatPos = []; DataMatNeg = [];
    for indCond = 1:4
        curMat = diff(squeeze(nanmean(DataMat(2,indCond,:,54,1:500),4)),1,2);
        %imagesc(curMat)
        for indID = 1:53
            curIdx = find(curMat(indID,:)>=0);
            DataMatPos(indCond,indID,:) = squeeze(nanmean(DataMat(2,indCond,indID,:,curIdx),5));
            curIdx = find(curMat(indID,:)<=0);
            DataMatNeg(indCond,indID,:) = squeeze(nanmean(DataMat(2,indCond,indID,:,curIdx),5));
        end
    end
%     imagesc(DataMatPos)
%     imagesc(DataMatNeg)
        
    h = figure('units','normalized','position',[.1 .1 .7 .3]);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(DataMatPos-DataMatNeg,2),1));
    ft_topoplotER(cfg,plotData);
    
        

    h = figure('units','normalized','position',[.1 .1 .7 .3]);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    time = dataResponseGrandAvg{1,1}.time;
    subplot(1,4,1);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT_diff(1,2:4,:,:,200:300),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    subplot(1,4,2);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT_diff(2,2:4,:,:,200:300),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
     subplot(1,4,3);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT_diff(1,2:4,:,:,300:350),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    subplot(1,4,4);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT_diff(2,2:4,:,:,300:350),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    
    h = figure('units','normalized','position',[.1 .1 .7 .3]);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    time = dataResponseGrandAvg{1,1}.time;
    subplot(1,4,1);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT(1,2:4,:,:,200:300),5),3),2)-...
            nanmean(nanmean(DataMat_alignedNDT(1,1,:,:,200:300),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    subplot(1,4,2);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT(2,2:4,:,:,200:300),5),3),2)-...
            nanmean(nanmean(DataMat_alignedNDT(2,1,:,:,200:300),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
     subplot(1,4,3);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT(1,2:4,:,:,300:350),5),3),2)-...
            nanmean(nanmean(DataMat_alignedNDT(1,1,:,:,300:350),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    subplot(1,4,4);
        DataMat_alignedNDT_diff = diff(DataMat_alignedNDT,1,5);
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_alignedNDT(2,2:4,:,:,300:350),5),3),2)-...
            nanmean(nanmean(DataMat_alignedNDT(2,1,:,:,300:350),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
   
    %% topography of effect
    
    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
    addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    %cfg.zlim = [-3 3];

%     figure;
%     plotData = [];
%     plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%     plotData.dimord = 'chan';
%     time = dataResponseGrandAvg{1,1}.time;
%     DataMat_diff = diff(DataMat,1,5);
%     subplot(2,2,1);
%         tmpData = squeeze(nanmedian(nanmedian(DataMat(1,2:4,:,:,:),2)-DataMat(1,1,:,:,:),3));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-150 & time <0),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('YA: -150:0 (234-1)')
%     subplot(2,2,2);
%         tmpData = squeeze(nanmedian(nanmedian(DataMat(2,2:4,:,:,:),2)-DataMat(2,1,:,:,:),3));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-150 & time <0),2));
%         ft_topoplotER(cfg,plotData);
%         title('OA: -150:0 (234-1)')
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%      subplot(2,2,3);
%         tmpData = squeeze(nanmedian(nanmedian(DataMat_diff(1,2:4,:,:,:),2)-DataMat_diff(1,1,:,:,:),3));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-150 & time <0),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('YA: -150:0 (234-1)')
%     subplot(2,2,4);
%         tmpData = squeeze(nanmedian(nanmedian(DataMat_diff(2,2:4,:,:,:),2)-DataMat_diff(2,1,:,:,:),3));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-150 & time <0),2));
%         ft_topoplotER(cfg,plotData);
%         title('OA: -150:0 (234-1)')
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)

    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    time = dataResponseGrandAvg{1,1}.time;
%     tmpData = squeeze(nanmean(zscore(DataMat(1,4,:,:,:),[],5)-zscore(DataMat(1,1,:,:,:),[],5),3));
%     plotData.powspctrm = squeeze(nanmean(tmpData(:,time>-100 & time <0),2));
%     ft_topoplotER(cfg,plotData);
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    time = dataResponseGrandAvg{1,1}.time;
    DataMat_diff = diff(DataMat,1,5);
    subplot(2,4,1)
        tmpData = squeeze(nanmean(nanmean(DataMat_diff(1,1,:,:,time>-250 & time <-100),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('YA: -250:-100 change 1')
    subplot(2,4,5)
        tmpData = squeeze(nanmedian(nanmean(DataMat_diff(1,1,:,:,time>-50 & time <0),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('YA: -50:0 change 1')
    subplot(2,4,2)
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_diff(1,2:4,:,:,time>-250 & time <-100),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('YA: -250:-100 change 234')
    subplot(2,4,6)
        tmpData = squeeze(nanmean(nanmean(nanmean(DataMat_diff(1,2:4,:,:,time>-50 & time <0),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('YA: -50:-0 change 234')
    subplot(2,4,3)
        tmpData = squeeze(nanmean(nanmean(DataMat_diff(2,1,:,:,time>-250 & time <-100),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('OA: -250:-100 change 1')
    subplot(2,4,7)
        tmpData = squeeze(nanmean(nanmean(DataMat_diff(2,1,:,:,time>-50 & time <0),5),3));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('OA: -50:0 change 1')
    subplot(2,4,4)
        tmpData = squeeze(nanmedian(nanmean(nanmean(DataMat_diff(2,2:4,:,:,time>-250 & time <-100),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('OA: -250:-100 change 234')
    subplot(2,4,8)
        tmpData = squeeze(nanmean(nanmean(nanmean(DataMat_diff(2,2:4,:,:,time>-50 & time <0),5),3),2));
        plotData.powspctrm = squeeze(tmpData);
        ft_topoplotER(cfg,plotData);
        title('OA -50:0 change 234')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/E_v3/'; mkdir(pn.plotFolder);
    figureName = 'E4_toposDiff';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
%     
%     
%     for indCond = 1:4
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{1,1}.time;
%         DataMat_diff = diff(DataMat,1,5);
%         subplot(2,4,indCond)
%         tmpData = squeeze(nanmedian(nanmedian(DataMat(1,indCond,:,:,time>-250 & time <100),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         subplot(2,4,4+indCond)
%         tmpData = squeeze(nanmedian(nanmedian(DataMat_diff(1,indCond,:,:,time>-250 & time <100),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     for indCond = 1:4
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{1,1}.time;
%         DataMat_diff = diff(DataMat,1,5);
%         subplot(3,4,indCond)
%         tmpData = squeeze(nanmedian(nanmedian(DataMat(1,indCond,:,:,time>-250 & time <100),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         subplot(3,4,4+indCond)
%         tmpData = squeeze(nanmedian(nanmedian(DataMat_diff(1,indCond,:,:,time>-250 & time <100),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         subplot(3,4,8+indCond)
%         DataMat_diff = diff(DataMat,2,5);
%         tmpData = squeeze(nanmedian(nanmedian(DataMat_diff(1,indCond,:,:,time>-250 & time <100),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{2,1}.time;
%         DataMat_diff = diff(DataMat,1,5);
%         tmpData = squeeze(nanmean(nanmean(DataMat_diff(1,indCond,:,:,time>-200 & time <-50),5)-nanmean(DataMat_diff(1,1,:,:,time>-200 & time <-50),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{1,1}.time;
%         tmpData = squeeze(nanmean(nanmedian(DataMat(1,indCond,:,:,time>-100 & time <0),5)-nanmedian(DataMat(1,indCond,:,:,time>-200 & time <-100),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{1,1}.time;
%         if indCond == 1
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:),3));
%         else
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:)-DataMat(1,1,:,:,:),3));
%         end
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-100 & time <0),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{1,1}.time;
%         if indCond == 1
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:),3));
%         else
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:)-DataMat(1,1,:,:,:),3));
%         end
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-200 & time <-100),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{1,1}.time;
%         if indCond == 1
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:),3));
%         else
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:)-DataMat(1,1,:,:,:),3));
%         end
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>0 & time <100),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
% 
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{1,1}.time;
%         if indCond == 1
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:),3));
%         else
%             tmpData = squeeze(nanmean(DataMat(1,indCond,:,:,:)-DataMat(1,1,:,:,:),3));
%         end
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>0 & time <400),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
% 
%     
%     
%     figure
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(zscore(DataMat(1,3,:,:),[],4)-zscore(DataMat(1,1,:,:),[],4)))
%     xlim([-800 150]);
%     
%     % get topography of this effect
%     
%     figure;
%     plotData = [];
%     plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%     plotData.dimord = 'chan';
%     time = dataResponseGrandAvg{1,1}.time;
%     tmpData = squeeze(nanmean(zscore(DataMat(1,4,:,:,:),[],5)-zscore(DataMat(1,1,:,:,:),[],5),3));
%     plotData.powspctrm = squeeze(nanmean(tmpData(:,time>-100 & time <0),2));
%     ft_topoplotER(cfg,plotData);
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{2,1}.time;
%         DataMat_diff = diff(DataMat,1,5);
%         tmpData = squeeze(nanmean(nanmean(DataMat_diff(1,indCond,:,:,time>-200 & time <-50),5)-nanmean(DataMat_diff(1,1,:,:,time>-200 & time <-50),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     figure;
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat_diff(1,3,:,:,:)-DataMat_diff(1,1,:,:,:),3)))
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmedian(DataMat_diff(1,3,:,:,:),3)))
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(1,3,:,:,:),3)))
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(1,3,:,:,:)-DataMat(1,1,:,:,:),3)))
%     xlim([-300 300])
%     
%     
%     
%     figure;
%     plotData = [];
%     plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%     plotData.dimord = 'chan';
%     time = dataResponseGrandAvg{1,1}.time;
%     tmpData = squeeze(nanmean(zscore(DataMat(1,4,:,:,:),[],5)-zscore(DataMat(1,1,:,:,:),[],5),3));
%     plotData.powspctrm = squeeze(nanmean(tmpData(:,time>-100 & time <0),2));
%     ft_topoplotER(cfg,plotData);
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     
%     for indCond = 1:4
%         subplot(1,4,indCond)
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{2,1}.time;
%         DataMat_diff = diff(DataMat,1,5);
%         tmpData = squeeze(nanmean(nanmean(DataMat_diff(1,indCond,:,:,time>-30 & time <-0),5)-nanmean(DataMat_diff(1,1,:,:,time>-30 & time <-0),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     %% more figures
%         
%     figure;
%     plotData = [];
%     plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%     plotData.dimord = 'chan';
%     time = dataResponseGrandAvg{1,1}.time;
%     DataMat_diff = diff(DataMat,1,5);
%     subplot(2,2,1);
%         tmpData = squeeze(nanmean(nanmedian(DataMat(1,2:4,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('YA: -50:0 vs -300:-250 (2:4)')
%     subplot(2,2,2);
%         tmpData = squeeze(nanmean(nanmedian(DataMat(2,2:4,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         title('OA: -50:0 vs -300:-250 (2:4)')
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%      subplot(2,2,3);
%         tmpData = squeeze(nanmean(nanmedian(DataMat_diff(1,2:4,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('YA: -50:0 vs -300:-250 (2:4)')
%     subplot(2,2,4);
%         tmpData = squeeze(nanmean(nanmedian(DataMat_diff(2,2:4,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         title('OA: -50:0 vs -300:-250 (2:4)')
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         
%     figure;
%     plotData = [];
%     plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%     plotData.dimord = 'chan';
%     time = dataResponseGrandAvg{1,1}.time;
%     DataMat_diff = diff(DataMat,1,5);
%     subplot(2,2,1);
%         tmpData = squeeze(nanmean(nanmedian(DataMat(1,1,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('YA: -50:0 vs -300:-250 (1)')
%     subplot(2,2,2);
%         tmpData = squeeze(nanmean(nanmedian(DataMat(2,1,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         title('OA: -50:0 vs -300:-250 (1)')
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%      subplot(2,2,3);
%         tmpData = squeeze(nanmean(nanmedian(DataMat_diff(1,1,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('YA: -50:0 vs -300:-250 (1)')
%     subplot(2,2,4);
%         tmpData = squeeze(nanmean(nanmedian(DataMat_diff(2,1,:,:,:),3),2));
%         plotData.powspctrm = squeeze(nanmedian(tmpData(:,time>-50 & time <0),2)-nanmedian(tmpData(:,time>-200 & time <-150),2));
%         ft_topoplotER(cfg,plotData);
%         title('OA: -50:0 vs -300:-250 (1)')
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         
%     for indCond = 1:4
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{2,1}.time;
%         DataMat_diff = diff(DataMat,1,5);
%         DataMatZ = zscore(DataMat,[],5);
%         subplot(2,4,indCond)
%         tmpData = squeeze(nanmean(nanmean(DataMatZ(1,indCond,:,:,time>-50 & time <0),5)-nanmean(DataMatZ(1,1,:,:,time>-50 & time <0),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         subplot(2,4,4+indCond)
%         tmpData = squeeze(nanmean(nanmean(DataMat_diff(1,indCond,:,:,time>-50 & time <0),5)-nanmean(DataMat_diff(1,1,:,:,time>-50 & time <0),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%     
%     figure;
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat_diff(1,3,:,:,:)-DataMat_diff(1,1,:,:,:),3)))
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmedian(DataMat_diff(1,3,:,:,:),3)))
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(1,3,:,:,:),3)))
%     imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(1,3,:,:,:)-DataMat(1,1,:,:,:),3)))
%     xlim([-300 300])
%     
%     figure;
%     plotData = [];
%     plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%     plotData.dimord = 'chan';
%         
%     for indCond = 1:4
%         plotData = [];
%         plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
%         plotData.dimord = 'chan';
%         time = dataResponseGrandAvg{2,1}.time;
%         DataMat_diff = diff(DataMat,1,5);
%         DataMatZ = zscore(DataMat,[],5);
%         subplot(2,4,indCond)
%         tmpData = squeeze(nanmean(nanmean(DataMatZ(1,indCond,:,:,time>-50 & time <0),5)-nanmean(DataMatZ(1,1,:,:,time>-50 & time <0),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         subplot(2,4,4+indCond)
%         tmpData = squeeze(nanmean(nanmean(DataMat_diff(1,indCond,:,:,time>-50 & time <0),5)-nanmean(DataMat_diff(1,1,:,:,time>-50 & time <0),5),3));
%         plotData.powspctrm = squeeze(tmpData);
%         ft_topoplotER(cfg,plotData);
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     end
%    

%% multiply change topography during evidence integration at higher loads with raw data

tmpData = squeeze(nanmean(nanmean(nanmean(DataMat_diff(1,1:4,:,:,time>-200 & time <-100),5),3),2));

DataMat2 = DataMat(1,:,:,:,:).*repmat(tmpData, 1,4,47,1,1001);
%idx_Chan = 53;
%idx_Chan = [12,44,53];
idx_Chan = [12];

% baseline @ 500 ms prior to response
time = dataResponseGrandAvg{1,1}.time;
DataMatBL = DataMat-repmat(nanmean(DataMat(:,:,:,:,time>-500 & time<-400),5),1,1,1,1,numel(time));

h = figure('units','normalized','position',[.1 .1 .3 .3]); 
cla; hold on;
    l1 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMatBL(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    l2 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMatBL(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    l3 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMatBL(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    l4 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMatBL(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    line([0, 0], [-4*10^-4, 3*10^-4], 'LineWidth', 2, 'Color', 'k', 'LineStyle', '--')
    ylim([-4*10^-4, 3*10^-4])
    xlabel('Time (ms); response-locked')
    legend([l1, l2, l3, l4], {'load 1'; 'load 2'; 'load 3'; 'load 4'}, 'location', 'SouthWest'); legend('boxoff');
    title({'Frontal ERP is transiently modulated prior to motor execution'; ''})
    xlim([-500 100])
    set(findall(gcf,'-property','FontSize'),'FontSize',14)


h = figure('units','normalized','position',[.1 .1 .3 .3]); 
cla; hold on;
    l1 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    l2 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    l3 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    l4 = plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2);
    line([0, 0], [-4*10^-4, 3*10^-4], 'LineWidth', 2, 'Color', 'k', 'LineStyle', '--')
    ylim([-4*10^-4, 3*10^-4])
    xlabel('Time (ms); response-locked')
    legend([l1, l2, l3, l4], {'load 1'; 'load 2'; 'load 3'; 'load 4'}, 'location', 'SouthWest'); legend('boxoff');
    title({'Frontal ERP is transiently modulated prior to motor execution'; ''})
    xlim([-500 100])
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    idx_Chan = [11	12	20	21	35	43	44	49]
    idx_Chan = [11	12	20	21]
    idx_Chan = [9	10	11	12	13	17	18	19	20	21]
    
h = figure('units','normalized','position',[.1 .1 .4 .4]); 
subplot(2,1,1); cla; hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(1,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    xlabel('Time (ms); response-locked')
    legend({'load 1'; 'load 2'; 'load 3'; 'load 4'}); legend('boxoff');
    title('YA: Bilateral parietal relative to response')
    xlim([-500 300])
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
subplot(2,1,2); cla; hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(2,1,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(2,2,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(2,3,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(DataMat(2,4,:,idx_Chan,:),4),3)), 'LineWidth', 2)
    xlim([-500 300])

    time = dataResponseGrandAvg{1,1}.time;
    
    figure; imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(1,1,:,idx_Chan,:),4)))
    figure; imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(1,2,:,idx_Chan,:),4)))
    figure; imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(1,4,:,idx_Chan,:),4)))
    figure; imagesc(dataResponseGrandAvg{1,1}.time,[],squeeze(nanmean(DataMat(2,4,:,idx_Chan,:),4)))
    
    % apply peak-finding mechanism to compare latency of change potential
    
    for indCond = 1:4
        for indID = 1:size(DataMat2,3)
            curData = squeeze(nanmean(DataMat(1,indCond,indID,idx_Chan,:),4));
            [maxVal, maxInd] = max([curData(480:550)]);
            PeakLatency(indCond,indID) = maxInd;
            PeakVal(indCond,indID) = maxVal;
            MeanVal(indCond,indID) = mean([curData(time>-200 & time<-50)]);
        end
    end
    figure; bar(nanmean(PeakLatency,2))
    figure; bar(nanmean(PeakVal,2))
    figure; bar(nanmean(MeanVal,2))
    
    [h, p] = ttest(PeakLatency(1,:), PeakLatency(3,:))
    [h, p] = ttest(MeanVal(1,:), MeanVal(2,:))
    [h, p] = ttest(MeanVal(2,:), MeanVal(3,:))
    [h, p] = ttest(MeanVal(3,:), MeanVal(4,:))
    [h, p] = ttest(MeanVal(2,:), MeanVal(4,:))
    [h, p] = ttest(MeanVal(1,:))
    
    % correlate with HDDM NDT estimates
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary.mat')

    DDMIDidx = ismember(cell2mat(cellfun(@str2num, HDDM_summary.IDs, 'un', 0)), cell2mat(cellfun(@str2num, IDs, 'un', 0)));
    DDMIDidx = find(DDMIDidx);

    curNDT = squeeze(HDDM_summary.nondecisionEEG(DDMIDidx,4))-squeeze(HDDM_summary.nondecisionEEG(DDMIDidx,1));
    curEEG = PeakLatency(4,:)-PeakLatency(1,:);
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)

    curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx,1:4),2));
    curEEG = squeeze(nanmean(PeakLatency(1:4,:),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    
    % part of NDT that belongs to motor preparation
    % assumption that motor preparation is close to zero duering simple execution (i.e. load 1)
    
    curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx,2:4)-repmat(HDDM_summary.nondecisionEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(PeakLatency(2:4,:)-repmat(PeakLatency(1,:),3,1),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    
    curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx,2:4)-repmat(HDDM_summary.nondecisionEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(PeakLatency(2:4,:)-repmat(PeakLatency(1,:),3,1),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    curNDT = squeeze(nanmean(HDDM_summary.driftEEG(DDMIDidx,2:4)-repmat(HDDM_summary.driftEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(PeakVal(2:4,:)-repmat(PeakVal(1,:),3,1),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx,2:4)-repmat(HDDM_summary.nondecisionEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(MeanVal(2:4,:)-repmat(MeanVal(1,:),3,1),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    curNDT = squeeze(nanmean(HDDM_summary.thresholdEEG(DDMIDidx,2:4)-repmat(HDDM_summary.thresholdEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(MeanVal(2:4,:)-repmat(MeanVal(1,:),3,1),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx,2:4)-repmat(HDDM_summary.nondecisionEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(MeanVal(2:4,:),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    
    curNDT = squeeze(nanmean(HDDM_summary.driftEEG(DDMIDidx,2:4),2));
    curEEG = squeeze(nanmean(MeanVal(2:4,:),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    curNDT = squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx,2:4)-repmat(HDDM_summary.nondecisionEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(PeakLatency(2:4,:)-repmat(PeakLatency(1,:),3,1),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)
    
    
    curNDT = squeeze(nanmean(HDDM_summary.thresholdEEG(DDMIDidx,2:4)-repmat(HDDM_summary.thresholdEEG(DDMIDidx,1),1,3),2));
    curEEG = squeeze(nanmean(PeakLatency(2:4,:)-repmat(PeakLatency(1,:),3,1),1));
    figure; scatter(curNDT,curEEG, 'filled')
    [r, p] = corrcoef(curNDT,curEEG)

    % bar plot of NDT
    figure; bar(squeeze(nanmean(HDDM_summary.nondecisionEEG(DDMIDidx,:),1)))
    
   
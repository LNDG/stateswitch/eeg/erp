%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

ERPstruct = [];
for id = 1:length(IDs)
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/H_ERPbyAcc/';
    load([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg');
    ERPstruct.dataStimAvg(:,:,id) = dataStimAvg;
end

cfg = [];
for indAcc = 1:2
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        dataStimGrandAvg{indCond,indAcc} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,indAcc,:});
    end
end

figure;
imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{1,1}.individual,1))); title('Stim-locked')
imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{4,1}.individual,1))); title('Stim-locked')
imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{4,2}.individual-dataStimGrandAvg{4,1}.individual,1))); title('Stim-locked')
imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{1,2}.individual-dataStimGrandAvg{1,1}.individual,1))); title('Stim-locked')

figure; hold on;
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,44:50,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,44:50,:),2),1))); title('Stim-locked')
figure; hold on;
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,44:50,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,44:50,:),2),1))); title('Stim-locked')

figure; hold on;
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,55:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,55:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,55:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,55:60,:),2),1))); title('Stim-locked')
legend({'L1', 'L2', 'L3', 'L4'})

figure; hold on;
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,40:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,40:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,40:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,40:60,:),2),1))); title('Stim-locked')
legend({'L1', 'L2', 'L3', 'L4'})

figure; hold on;
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,58:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,58:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,58:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,58:60,:),2),1))); title('Stim-locked')

figure; hold on;
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,1:60,:)-dataStimGrandAvg{1,1}.individual(:,1:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,1:60,:)-dataStimGrandAvg{2,1}.individual(:,1:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,1:60,:)-dataStimGrandAvg{3,1}.individual(:,1:60,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,1:60,:)-dataStimGrandAvg{4,1}.individual(:,1:60,:),2),1))); title('Stim-locked')


figure; hold on;
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,44:50,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,44:50,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,44:50,:),2),1))); title('Stim-locked')
plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,44:50,:),2),1))); title('Stim-locked')


plot(dataStimGrandAvg{1,1}.time, squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,44:50,:),2),1))); title('Stim-locked')



ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1:2
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        dataStimGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,ageIdx{indAge}});
    end
end

figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{1,1}.individual,1))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataResponseGrandAvg{1,1}.individual,1))); title('Response-locked')

figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataStimGrandAvg{1,1}.individual,1))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataResponseGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataResponseGrandAvg{1,1}.individual,1))); title('Response-locked')


figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{2,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{3,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{4,1}.individual,1))-squeeze(nanmean(dataProbeGrandAvg{1,1}.individual,1))); title('Response-locked')


figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,1}.time, [], squeeze(nanmean(dataStimGrandAvg{1,1}.individual(:,46,:),2))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,1}.time, [], squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),2))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),2))); title('Response-locked')

figure; 
subplot(1,3,1); imagesc(dataStimGrandAvg{1,2}.time, [], squeeze(nanmean(dataStimGrandAvg{1,2}.individual(:,46,:),2))); title('Stim-locked')
subplot(1,3,2); imagesc(dataProbeGrandAvg{1,2}.time, [], squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),2))); title('Probe-locked')
subplot(1,3,3); imagesc(dataResponseGrandAvg{1,2}.time, [], squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),2))); title('Response-locked')


% figure; 
% subplot(1,3,1); imagesc(dataStimGrandAvg{1}.time, [], dataStimGrandAvg{1}.avg); title('Stim-locked')
% subplot(1,3,2); imagesc(dataProbeGrandAvg{1}.time, [], dataProbeGrandAvg{1}.avg); title('Probe-locked')
% subplot(1,3,3); imagesc(dataResponseGrandAvg{1}.time, [], dataResponseGrandAvg{1}.avg); title('Response-locked')


h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3) relative to stimulus onset')
subplot(2,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P6) relative to stimulus onset')
subplot(2,2,3); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3) relative to stimulus onset')
subplot(2,2,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P6) relative to stimulus onset')
    
h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,2,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[45],:)-dataStimGrandAvg{1,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[45],:)-dataStimGrandAvg{1,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45],:)-dataStimGrandAvg{1,1}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3) relative to stimulus onset')
subplot(2,2,2); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[49],:)-dataStimGrandAvg{1,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[49],:)-dataStimGrandAvg{1,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[49],:)-dataStimGrandAvg{1,1}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P6) relative to stimulus onset')
subplot(2,2,3); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[45],:)-dataStimGrandAvg{1,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[45],:)-dataStimGrandAvg{1,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45],:)-dataStimGrandAvg{1,2}.individual(:,[45],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3) relative to stimulus onset')
subplot(2,2,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[49],:)-dataStimGrandAvg{1,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[49],:)-dataStimGrandAvg{1,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[49],:)-dataStimGrandAvg{1,2}.individual(:,[49],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P6) relative to stimulus onset')


%% plot topography 

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-3 3];

figure;
subplot(2,3,1)
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,2)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,1}.time>200 & dataProbeGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,3)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,3,4)
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,2}.time>1000 & dataStimGrandAvg{1,2}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,5)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,2}.time>200 & dataProbeGrandAvg{1,2}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,6)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,2}.time>-100 & dataResponseGrandAvg{1,2}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    %% 4-1 contrast
    
figure;
subplot(2,3,1)
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,:,idxTime)-dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,2)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,1}.time>200 & dataProbeGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{4,1}.individual(:,:,idxTime)-dataProbeGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,3)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{4,1}.individual(:,:,idxTime)-dataResponseGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,3,4)
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,2}.time>1000 & dataStimGrandAvg{1,2}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,:,idxTime)-dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,5)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,2}.time>200 & dataProbeGrandAvg{1,2}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{4,2}.individual(:,:,idxTime)-dataProbeGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,6)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,2}.time>-100 & dataResponseGrandAvg{1,2}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{4,2}.individual(:,:,idxTime)-dataResponseGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
%% YA vs OA contrast

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45 49],:),2),1)), 'LineWidth', 2)
    xlim([-200 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target YA'; '4 Targets YA'; '1 Target OA'; '4 Targets OA'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(1,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,46,:),1)), 'LineWidth', 2)
    xlim([-200 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(1,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),2),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{4,1}.individual(:,46,:),2),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),2),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataResponseGrandAvg{4,2}.individual(:,46,:),2),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% cumsum

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,cumsum(squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[45 49],:),2),1))), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,46,:),1))), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,cumsum(squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,46,:),1))), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% Check for potential integration onset differences

timeIntervals = [150 200 250 300 350 400 450];
for indCond = 1:4
    figure;
    for indTime = 1:numel(timeIntervals)-1
        subplot(1,numel(timeIntervals)-1, indTime);
        plotData = [];
        plotData.label = dataProbeGrandAvg{indCond,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        idxTime = dataProbeGrandAvg{indCond,1}.time>timeIntervals(indTime) & ...
            dataProbeGrandAvg{indCond,1}.time<timeIntervals(indTime+1);
        plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{indCond,1}.individual(:,:,idxTime),3),1))';
        ft_topoplotER(cfg,plotData);
        %title('YA ERP @ +200:+300 probe, 1 Target')
    end
end


figure;
subplot(2,3,1)
    plotData = [];
    plotData.label = dataStimGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,1}.time>1000 & dataStimGrandAvg{1,1}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,2)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,1}.time>200 & dataProbeGrandAvg{1,1}.time<300;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,3)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,1}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('YA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,3,4)
    plotData = [];
    plotData.label = dataStimGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataStimGrandAvg{1,2}.time>1000 & dataStimGrandAvg{1,2}.time<3000;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +1000:+300 stim, 1 Target')
subplot(2,3,5)
    plotData = [];
    plotData.label = dataProbeGrandAvg{1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataProbeGrandAvg{1,2}.time>300 & dataProbeGrandAvg{1,2}.time<350;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataProbeGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ +200:+300 probe, 1 Target')
subplot(2,3,6)
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxTime = dataResponseGrandAvg{1,2}.time>-100 & dataResponseGrandAvg{1,2}.time<100;
    plotData.powspctrm = squeeze(nanmean(nanmean(dataResponseGrandAvg{1,2}.individual(:,:,idxTime),3),1))';
    ft_topoplotER(cfg,plotData);
    title('OA ERP @ -100:+100 peri-response, 1 Target')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
%% CPP plot at maximum positive and negative CPP locations
    
h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{1,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{2,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{3,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[54],:),2)-nanmean(dataStimGrandAvg{4,1}.individual(:,[28],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ POz) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,54,:)-dataProbeGrandAvg{1,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,54,:)-dataProbeGrandAvg{2,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,54,:)-dataProbeGrandAvg{3,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,54,:)-dataProbeGrandAvg{4,1}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ POz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,54,:)-dataResponseGrandAvg{1,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,54,:)-dataResponseGrandAvg{2,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,54,:)-dataResponseGrandAvg{3,1}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,54,:)-dataResponseGrandAvg{4,1}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ POz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{1,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{2,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{3,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,54,:),2)-nanmean(dataStimGrandAvg{4,2}.individual(:,28,:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ POz) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,54,:)-dataProbeGrandAvg{1,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,54,:)-dataProbeGrandAvg{2,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,54,:)-dataProbeGrandAvg{3,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,54,:)-dataProbeGrandAvg{4,2}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ POz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,54,:)-dataResponseGrandAvg{1,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,54,:)-dataResponseGrandAvg{2,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,54,:)-dataResponseGrandAvg{3,2}.individual(:,28,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,54,:)-dataResponseGrandAvg{4,2}.individual(:,28,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ POz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[54],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,54,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,54,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,3,1); hold on;
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,1}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,1}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('YA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,2); hold on;
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{1,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{2,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{3,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,1}.time,squeeze(nanmean(dataProbeGrandAvg{4,1}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('YA: CPP (@ Pz) relative to probe onset')
subplot(2,3,3); hold on;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA: CPP (@ Pz) relative to response')
subplot(2,3,4); hold on;
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{1,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{2,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{3,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    plot(dataStimGrandAvg{1,2}.time,squeeze(nanmean(nanmean(dataStimGrandAvg{4,2}.individual(:,[29],:),2),1)), 'LineWidth', 2)
    xlim([0 3000]); xlabel('Time (ms); stim-locked')
    legend({'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}); legend('boxoff')
    title('OA: CPP (@ P3 & P6) relative to stimulus onset')
subplot(2,3,5); hold on;
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{1,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{2,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{3,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataProbeGrandAvg{1,2}.time,squeeze(nanmean(dataProbeGrandAvg{4,2}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([0 1000]); xlabel('Time (ms); probe-locked')
    title('OA: CPP (@ Pz) relative to probe onset')
subplot(2,3,6); hold on;
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,29,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,29,:),1)), 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('OA: CPP (@ Pz) relative to response')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% plot YA and OA in same plot

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,3,1); hold on;
    indChannel = 29;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,indChannel,:),1)), '--','LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA + OA: Negative CPP (@ Cz) relative to response')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,3,2); hold on;
    indChannel = 54;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,indChannel,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,indChannel,:),1)),'--', 'LineWidth', 2)
    line([0 0], get(gca, 'Ylim'))
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA + OA: Positive CPP (@ POz) relative to response')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,3,3); hold on;
    indChannel1 = 54;
    indChannel2 = 29;
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{1,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{1,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{2,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{2,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{3,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{3,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,1}.time,squeeze(nanmean(dataResponseGrandAvg{4,1}.individual(:,indChannel1,:)-dataResponseGrandAvg{4,1}.individual(:,indChannel2,:),1)), 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{1,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{1,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{2,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{2,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{3,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{3,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    plot(dataResponseGrandAvg{1,2}.time,squeeze(nanmean(dataResponseGrandAvg{4,2}.individual(:,indChannel1,:)-dataResponseGrandAvg{4,2}.individual(:,indChannel2,:),1)),'--', 'LineWidth', 2)
    xlim([-800 150]); xlabel('Time (ms); response-locked')
    title('YA + OA: Positive-Negative CPP')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% compare topographies between -400,-200 and -200,0

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-3 3];

    figure;
    subplot(2,4,3);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-400 & dataResponseGrandAvg{1,1}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 4 -400 to -200')
    
    subplot(2,4,4);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 4 -200 to 0')
    
    subplot(2,4,1);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-400 & dataResponseGrandAvg{1,1}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 1 -400 to -200')
    
    subplot(2,4,2);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,1}.individual(:,:,dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('YA: Condition 1 -200 to 0')
    
   
 %% OA
 
    subplot(2,4,7);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-400 & dataResponseGrandAvg{1,2}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 4 -400 to -200')
    
    subplot(2,4,8);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{4,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-200 & dataResponseGrandAvg{1,2}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 4 -200 to 0')
    
    subplot(2,4,5);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-400 & dataResponseGrandAvg{1,2}.time<-200),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 1 -400 to -200')
    
    subplot(2,4,6);
    plotData = [];
    plotData.label = dataResponseGrandAvg{1,2}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(diff(dataResponseGrandAvg{1,2}.individual(:,:,dataResponseGrandAvg{1,2}.time>-200 & dataResponseGrandAvg{1,2}.time<-0),[],3),3),1))';
    ft_topoplotER(cfg,plotData);
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('OA: Condition 1 -200 to 0')

    %% individual slope fits
    
    figure;
    subplot(2,4,1); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{1,1}.individual(:,54,:)))
    subplot(2,4,2); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{2,1}.individual(:,54,:)))
    subplot(2,4,3); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{3,1}.individual(:,54,:)))
    subplot(2,4,4); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{4,1}.individual(:,54,:)))
    subplot(2,4,5); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{1,2}.individual(:,54,:)))
    subplot(2,4,6); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{2,2}.individual(:,54,:)))
    subplot(2,4,7); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{3,2}.individual(:,54,:)))
    subplot(2,4,8); imagesc(dataResponseGrandAvg{1,1}.time, [], squeeze(dataResponseGrandAvg{4,2}.individual(:,54,:)))

    
    % slope fit from -250 to -100 (McGovern et al., 2018)
    
    dataForSlopeFit = []; slopes = [];
    for indAge = 1:2
        for indCond = 1:4
            dataForSlopeFit{indAge}(:,indCond,:) = squeeze(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-300 & dataResponseGrandAvg{1,1}.time<-0));
            for indID = 1:size(dataForSlopeFit{indAge},1)
                %slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))',1);
                slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);
                %scatter(1:size(dataForSlopeFit{indAge},3), zscore(squeeze(dataForSlopeFit{indAge}(indID,indCond,:)))', 'filled'); pause(.2)
            end
        end
    end
    
    figure; 
    subplot(3,2,1); bar([nanmean(squeeze(slopes{1}(:,:,1)),1)]); title('Slope YA')
    subplot(3,2,2); bar([nanmean(squeeze(slopes{1}(:,:,2)),1)]); title('Intercept YA')
    subplot(3,2,3); bar([nanmean(squeeze(slopes{2}(:,:,1)),1)]); title('Slope OA')
    subplot(3,2,4); bar([nanmean(squeeze(slopes{2}(:,:,2)),1)]); title('Intercept OA')
    suptitle({'CPP slopes and intercepts -300:0 (cf. McGovern et al.)'})
    subplot(3,2,[5 6]); hold on;
    for indCond = 1:4
        scatter(squeeze(slopes{1}(:,indCond,1)),squeeze(slopes{1}(:,indCond,2)), 'filled');
    end
    title({'Note that CPP slope is inter-individually';'strongly correlated with CPP magnitude @ -300'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    legend({'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'})

    % correlate with individual drift rate estimates
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_v12_a2_t2.mat')

    idx_YA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{1}));
    idx_OA = ismember(HDDM_summary_v12_a2_t2.IDs, IDs(ageIdx{2}));
    
    figure; 
    subplot(2,2,1); hold on;
    for indCond = 1:4
        l{indCond} = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,indCond),squeeze(slopes{1}(:,indCond,1)), 'filled'); lsline();
    end; title('CPP Slope YA')
    subplot(2,2,2); hold on;
    for indCond = 1:4
        scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,indCond),squeeze(slopes{2}(:,indCond,1)), 'filled'); lsline();
    end; title('CPP Slope OA')
    subplot(2,2,3); hold on;
    for indCond = 1:4
        scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,indCond),squeeze(slopes{1}(:,indCond,2)), 'filled'); lsline();
    end; title('CPP Intercept YA')
    subplot(2,2,4); hold on;
    for indCond = 1:4
        scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,indCond),squeeze(slopes{2}(:,indCond,2)), 'filled'); lsline();
    end; title('CPP Intercept OA')
    legend([l{1},l{2},l{3},l{4}], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'});
   	set(findall(gcf,'-property','FontSize'),'FontSize',18)

    figure;
    subplot(1,2,1); hold on;
        l1 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,1)), 'filled'); lsline();
        l2 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,1)), 'filled'); lsline();
        [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,1)));
        Corr1 = r(2); P1 = p(2);
        [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,1)));
        Corr2 = r(2); P2 = p(2);
        legend([l1,l2], {['YA: r = ', num2str(round(Corr1,2)), '; p = ', num2str(round(P1,3))],...
            ['OA: r = ', num2str(round(Corr2,2)), '; p = ' num2str(round(P2,3))]})
        title('CPP slope correlates with estimated drift in Load 1')
        xlabel('Drift estimate'); ylabel('CPP Slope -300 to 0');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
    subplot(1,2,2); hold on;
        l1 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,2)), 'filled'); lsline();
        l2 = scatter(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,2)), 'filled'); lsline();
        [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1),squeeze(slopes{1}(:,1,2)));
        Corr1 = r(2); P1 = p(2);
        [r,p] = corrcoef(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1),squeeze(slopes{2}(:,1,2)));
        Corr2 = r(2); P2 = p(2);
        legend([l1,l2], {['YA: r = ', num2str(round(Corr1,2)), '; p = ', num2str(round(P1,3))],...
            ['OA: r = ', num2str(round(Corr2,2)), '; p = ' num2str(round(P2,3))]})
        title('CPP intercept correlates with estimated drift in Load 1')
        xlabel('Drift estimate'); ylabel('CPP Intercept @ -300');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)

    %% get peak as threshold estimate
    
    CPPthreshold = [];
    for indAge = 1:2
        for indCond = 1:4
            CPPthreshold{indAge}(:,indCond) = squeeze(nanmean(dataResponseGrandAvg{indCond,indAge}.individual(:,54,dataResponseGrandAvg{1,1}.time>-100 & dataResponseGrandAvg{1,1}.time<100),3));
        end
    end
    
    figure; 
    bar([nanmean(squeeze(CPPthreshold{1}(:,:)),1), nanmean(squeeze(CPPthreshold{2}(:,:)),1)]); title('Thresholds YA, OA')
    
    figure; 
    subplot(1,2,1); hold on;
    for indCond = 1:4
        l{indCond} = scatter(HDDM_summary_v12_a2_t2.thresholdEEG(idx_YA,indCond),squeeze(CPPthreshold{1}(:,indCond)), 'filled'); lsline();
    end; title('CPP Threshold YA')
    subplot(1,2,2); hold on;
    for indCond = 1:4
        scatter(HDDM_summary_v12_a2_t2.thresholdEEG(idx_OA,indCond),squeeze(CPPthreshold{2}(:,indCond)), 'filled'); lsline();
    end; title('CPP Threshold OA')
    legend([l{1},l{2},l{3},l{4}], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'});
   	set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    %% consider slope (i.e. change in CPP) vs. duration of CPP
    